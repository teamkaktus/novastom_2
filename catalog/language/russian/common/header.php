<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Мои Закладки (%s)';
$_['text_shopping_cart'] = 'Корзина покупок';
$_['text_category']      = 'Категории';
$_['text_account']       = 'Личный кабинет';
$_['text_register']      = 'Регистрация';
$_['text_login']         = 'Авторизация';
$_['text_order']         = 'История заказов';
$_['text_transaction']   = 'История платежей';
$_['text_download']      = 'Файлы для скачивания';
$_['text_logout']        = 'Выход';
$_['text_checkout']      = 'Оформление заказа';
$_['text_search']        = 'Поиск';
$_['text_all']           = 'Показать все';
$_['text_page']          = 'страница';

// Error
$_['error_exists']         = 'Этот E-Mail уже зарегистрирован!';
$_['error_firstname']      = 'Имя должно содержать от 1 до 32 символов!';
$_['error_lastname']       = 'Фамилия должна содержать от 1 до 32 символов!';
$_['error_middle_name']       = 'Отчество должна содержать от 1 до 32 символов!';
$_['error_email']          = 'E-Mail введён неправильно!';
$_['error_telephone']      = 'В телефоне должно быть от 3 до 32 цифр!';
$_['error_address_1']      = 'Адрес должен содержать от 3 до 128 символов!';
$_['error_city']           = 'Название города должно содержать от 2 до 128 символов!';
$_['error_postcode']       = 'В индексе должно быть от 2 до 10 символов!';
$_['error_country']        = 'Выберите страну!';
$_['error_zone']           = 'Выберите регион / область!';
$_['error_custom_field']   = '%s необходим!';
$_['error_password']       = 'В пароле должно быть от 4 до 20 символов!';
$_['error_confirm']        = 'Пароли не совпадают!';
$_['error_agree']          = 'Для регистрации Вы должны быть согласны с документом %s!';