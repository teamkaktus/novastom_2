<?php echo $header; ?>
<div class="content clearfix">
  <div class="container">
    <div class="breadcrumb breadcrumb--bd breadcrumb--sd">
      <ul class="breadcrumb-list">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li>
          <a href="<?php echo $breadcrumb['href']; ?>">
            <?php echo $breadcrumb['text']; ?>
          </a>
        </li>
        <?php } ?>
      </ul>
    </div>
    <div class="row">
      <?php echo $column_left; ?>
      <?php if ($column_left && $column_right) { ?>
      <?php $class = 'col-sm-6'; ?>
      <?php } elseif ($column_left || $column_right) { ?>
      <?php $class = 'col-sm-9'; ?>
      <?php } else { ?>
      <?php $class = 'col-sm-12'; ?>
      <?php } ?>
      <div class="main">
        <div id="content" class="<?php echo $class; ?> padd_0_sm_9">
          <?php echo $content_top; ?>
          <div class="nav-mob">
            <div id="nav-catalog" class="nav-mob__item nav-btn nav-btn--catalog">
              <div class="nav-btn__wrap nav-btn__wrap--catalog">
                <b></b>
                <b></b>
                <b></b>
              </div>
              <span>Каталог</span>
            </div>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <div class="nav-mob__item nav-mob__item--in">
              <a href="<?php echo $breadcrumb['href']; ?>">
                <?php echo $breadcrumb['text']; ?>
              </a>
            </div>
            <?php } ?>
            <div class="nav-mob__item nav-mob__item--end">
              <?php echo $heading_title; ?>
            </div>
          </div>
          <div class="page-title page-title--catalog">
            <?php echo $heading_title; ?>
          </div>

          <div class="row">
            <?php foreach ($categories_ch as $category_ch) { ?>
            <div class="category_special_container">
              <div class="category_special_image"><a href="<?php echo $category_ch['href']; ?>">
                <img src="<?php echo $category_ch['image']; ?>" alt="<?php echo $category_ch['name']; ?>" title="<?php echo $category_ch['name']; ?>" class="img-responsive category_special_image" /></a></div>
              <div>
                <div class="category_special_name">
                  <a href="<?php echo $category_ch['href']; ?>">
                    <?php echo $category_ch['name']; ?>
                  </a>
                </div>

              </div>
            </div>
            <?php } ?>
          </div>
          <div class="row">
            <div class="pagination">
              <?php echo $pagination; ?>
            </div>
          </div>

          <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>