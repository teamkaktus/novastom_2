<?php echo $header; ?>
<div class="content clearfix">
    <div class="container">
        <div class="breadcrumb--bd breadcrumb--bd__style hidden_breadcrumb">
            <ul class="breadcrumb-list breadcrumb-list_style">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
        <div class="nav-mob">
            <div id="nav-catalog" class="nav-mob__item nav-btn nav-btn--catalog nav-mob_product_new">
                <div class="nav-btn__wrap nav-btn__wrap--catalog">
                    <b></b>
                    <b></b>
                    <b></b>
                </div>
                <span>Каталог</span>
            </div>
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <div class="nav-mob__item nav-mob__item--in nav-mob__item--in_product_new ">
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </div>
            <?php } ?>
            <div class="nav-mob__item nav-mob__item--end"><?php echo $breadcrumb['text']; ?></div>
        </div>
        <div class="container_title_catalog">
            <span class="text_title_catalog">КАТАЛОГ ТОВАРОВ</span>
        </div>
        <div class="container_catalog_style2">
            <?php foreach ($categories_ch as $category_ch) { ?>
            <a href="<?php echo $category_ch['href']; ?>">
            <div class="container__catalog ">
                
                <div class="container_image_catalog">
                    
                        <img class="image_style_catalog" src="<?php echo $category_ch['image']; ?>"
                             alt="<?php echo $category_ch['name']; ?>"
                             title="<?php echo $category_ch['name']; ?>"/>
                    
                </div>
                <div class="container_name_catalog">
                    <a href="<?php echo $category_ch['href']; ?>">
                        <span class="text_name_tovar_catalog"><?php echo $category_ch['name']; ?></span>
                    </a>
                </div>
               
            </div>
             </a>
            <?php } ?>
        </div>
        <?php echo $content_bottom; ?>
    </div>
</div>
<?php echo $footer; ?>
