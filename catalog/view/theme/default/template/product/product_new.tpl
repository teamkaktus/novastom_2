<?php echo $header; ?>
<div class="content content--tovar clearfix">
  <div class="container main_container_product_new_mob">
    <div class="breadcrumb breadcrumb--bd">
      <ul class="breadcrumb-list">
         <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php if ($breadcrumb ['href']==('')){ ?>
            <li><?php echo $breadcrumb['text']; ?></li>
            <?php }else{ ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
            <?php } ?>
      </ul>
    </div>
    <div class="navigation_panel_product_new">
     <?php foreach ($categories_ch as $category_ch) { ?>
      <div class="navigation_panel_container_name_product_new">
        <a href="<?php echo $category_ch['href']; ?>">
          <span class="navigation_panel_text_name_tovar_product_new"><?php echo $category_ch['name']; ?></span>
        </a>
      </div>
      <?php } ?>
    </div>
    <div style="clear: both"></div>
    <div class="row row_product_new">
      <div class="column_left_product_new">
        <?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
      </div>
      <div class="nav-mob">
        <div id="nav-catalog" class="nav-mob__item nav-btn nav-btn--catalog nav-mob_product_new">
          <div class="nav-btn__wrap nav-btn__wrap--catalog">
            <b></b>
            <b></b>
            <b></b>
          </div>
          <span>Каталог</span>
        </div>
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <div class="nav-mob__item nav-mob__item--in nav-mob__item--in_product_new ">
          <a href="<?php echo $breadcrumb['href']; ?>">
            <?php echo $breadcrumb['text']; ?>
          </a>
        </div>
        <?php } ?>
        <div class="nav-mob__item nav-mob__item--end">
          <?php echo $breadcrumb['text']; ?>
        </div>
      </div>
      <?php $array_chunk_options = array_chunk($options, 10, TRUE); ?>
      <?php if ($options && $type_product) { ?>

<!-- Mob_start -->
      <div class="container_product_new_mob main_container_product_new_mob">
        <div class="container_title_product_new">
          <?php echo $heading_title; ?>
        </div>
        <div class="container_title_product_new1">
          <a href="" title="<?php echo $heading_title; ?>"><img class="thumbnail_product_new"
                                                                src="<?php echo $thumb; ?>"
                                                                title="<?php echo $heading_title; ?>"
                                                                alt="<?php echo $heading_title; ?>"/></a>
        </div>
        <div id="product">


          <div class="container_title_product_new2 hidden">

            <?php $image_option = ''; ?>
            <?php foreach ($options as $option) { ?>
            <?php if ($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') { ?>
            <?php foreach ($option['product_option_value'] as $option_value) { ?>
            <?php if($image_option != $option_value['image']){ ?>
            <?php $image_option = $option_value['image']; ?>
            <img class="product_new_image_option" src="<?php echo $image_option; ?>" />
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php } ?>
          </div>
          <?php $max_width = 100; ?>
          <?php $result_option_sort_order = ''; ?>
          <?php $max_option_sort_order = max(array($option_value['option_sort_order'])); ?>
          <?php $result_option_sort_order = $max_width / $max_option_sort_order; ?>

          <div class="">
            <div class="">
              <table class="table_style_product_new_mob">
                <tr class="text_table_product_new2">
                  <td class="text_table_product_new_mob mob_title_td_style" colspan="6">Форма</td>
                </tr>
                <tr class="heidch_tr heidch_tr_mob">
                  <?php foreach ($additions as $addition) { ?>
                  <td class="text_table_product_new4" style="width: <?php echo $result_option_sort_order ; ?>%">
                    <?php echo $addition['addition_form']; ?>
                  </td>
                  <?php } ?>
                </tr>
                <tr>
                  <td class="text_table_product_new_mob mob_title_td_style" colspan="6">Тип</td>
                </tr>
                <tr class="heidch_tr heidch_tr_mob">
                  <?php foreach ($additions as $addition) { ?>
                  <td class="text_table_product_new4">
                    <?php echo $addition['addition_type']; ?>
                  </td>
                  <?php } ?>
                </tr>
                <tr class="text_table_product_new2">
                  <td class="text_table_product_new_mob mob_title_td_style" colspan="6">Диаметр</td>
                </tr>
              </table>
              <tr>
                <div class="container_mob_table">
                  <?php foreach ($options as $option) { ?>
                  <?php if ($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td' ) { ?>
                  <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                    <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option">
                      <table>
                        <thead>
                          <tr>
                            <?php if (!empty($option['owq_has_image'])) { ?>
                            <td class="hidden">Изображения</td>
                            <?php } ?>
                            <td class="td_style_hidden<?php echo $option['product_option_id']; ?> option_name_product_new" colspan="2"><span><?php echo $option['name']; ?></span></td>
                            <?php if (!empty($option['owq_has_sku'])) { ?>
                            <td class="hidden">Артикул:</td>
                            <?php } ?>
                            <?php if (!empty($option['owq_has_stock'])) { ?>
                            <td class="hidden">Наличие:</td>
                            <?php } ?>
                            <td class="hidden">Цена:</td>
                            <?php if (!empty($option['owq_discounts'])) { ?>
                            <?php foreach ($option['owq_discounts'] as $dvalue) { ?>
                            <td>Цена от<br />
                              <?php echo $dvalue; ?>
                            </td>
                            <?php } ?>
                            <?php } ?>
                            <td class="col-quantity hidden">
                              <?php echo $entry_qty; ?>:</td>
                          </tr>
                        </thead>
                        <tbody class="tbody_hidden<?php echo $option['product_option_id']; ?> hidden">
                          <script>
                            $(document).ready(function () {
                              $(".td_style_hidden<?php echo $option['product_option_id']; ?>").click(function () {
                                $(".tbody_hidden<?php echo $option['product_option_id']; ?>").slideToggle(
                                  "slow");
                              });
                            });
                          </script>
                          <?php foreach ($option['product_option_value'] as $option_value) { ?>
                          <tr class="float-left border_style_product_new" style="width: <?php echo $result_option_sort_order ; ?>%">
                            <?php if (!empty($option['owq_has_image'])) { ?>
                            <td class="hidden">
                              <?php if (!empty($option_value['image'])) { ?><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name']; ?>"
                                class="img-thumbnail" />
                              <?php } ?>
                            </td>
                            <?php } ?>
                            <td class="hidden">
                              <input style="display:none;" type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="" data-split="/"
                                data-price="0" data-prefix="=" data-fprice="<?php echo $option_value['owq_full_price']; ?>"
                                data-id="<?php echo $option_value['product_option_value_id']; ?>" />
                              <?php echo $option_value['name']; ?>
                            </td>
                            <?php if (!empty($option['owq_has_sku'])) { ?>
                            <td class="hidden">
                              <?php echo $option_value['owq_sku']; ?>
                            </td>
                            <?php } ?>
                            <?php if (!empty($option['owq_has_stock'])) { ?>
                            <td class="stock hidden">
                              <?php if (!empty($option_value['owq_has_stock'])) { ?>
                              <?php echo $option_value['owq_quantity']; ?> шт.
                              <?php } ?>
                            </td>
                            <?php } ?>
                            <td class="hidden">
                              <?php if ($option_value['owq_full_price_text']) { ?>
                              <?php echo $option_value['owq_full_price_text']; ?>
                              <?php } ?>
                            </td>
                            <?php if (!empty($option_value['owq_discounts'])) { ?>
                            <?php foreach ($option_value['owq_discounts'] as $dvalue) { ?>
                            <td class="hidden">
                              <?php echo $dvalue; ?>
                            </td>
                            <?php } ?>
                            <?php } ?>

                            <td class="col-quantity">
                              <div class="owq-quantity mob"><span class="owq-sub"></span><input style="background-image: url(<?php echo $option_value['image_option']; ?>); background-position: center; border: 1px solid #12465d; border-radius: 5px; color: #fff;"
                                  type="text" value="0" <?php if (!empty($option_value[ 'owq_has_stock'])) { ?>data-max="
                                <?php echo $option_value['owq_quantity']; ?>"
                                <?php } ?> class="form-control owq-input" /><span class="owq-add"></span></div>
                            </td>

                          </tr>
                          <?php } ?>
                          <?php $length_option = ''; ?>
                          <?php foreach ($option['product_option_value'] as $option_value) { ?>
                          <?php if($length_option != $option_value['length_option']){ ?>
                          <?php $length_option = $option_value['length_option']; ?>
                          <td class="text_length_option" style="width=" 50% "">
                            <span class="text_mob_length">Длина рабочей части</span></br>
                            <span class="text_mob_length_option"><?php echo $length_option; ?></span>
                          </td>
                          <?php } ?>
                          <?php } ?>
                          <?php $price_option = ''; ?>
                          <?php foreach ($option['product_option_value'] as $option_value) { ?>
                          <?php if($price_option != $option_value['price']){ ?>
                          <?php $price_option = $option_value['price']; ?>
                          <td class="text_length_option" style="width=" 50% "">
                            <span class="text_mob_length">Цена за 1 шт.</span></br>
                            <span class="text_mob_price_option"><?php echo $price_option; ?><span class="text_mob_price_option"> руб</span></span>
                          </td>
                          <?php } ?>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <?php $entry_qty = ''; ?>
                  <?php } ?>
                  <?php } ?>
                </div>
              </tr>


              </table>

            </div>

          </div>
          <div class="legend">
            <?php if ($attribute_groups) { ?>
            <?php foreach ($attribute_groups as $attribute_group) { ?>
            <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
            <div class="title_legend">
              <?php echo $attribute['text']; ?>
            </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php foreach ($additions as $addition) { ?>
            <div class="container_data_legend">
              <img class="image_legend" src="<?php echo $addition['addition_image']; ?>" alt="">
              <div class="name_attribute_legend">
                <?php echo $addition['addition_name']; ?>
              </div>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
      <!-- Mob_end -->


      <div class="container_product_new">
        <div class="container_title_product_new">
          <?php echo $heading_title; ?>
        </div>
        <div class="container_title_product_new1">
          <a href="" title="<?php echo $heading_title; ?>"><img class="thumbnail_product_new"
                                                                src="<?php echo $thumb; ?>"
                                                                title="<?php echo $heading_title; ?>"
                                                                alt="<?php echo $heading_title; ?>"/></a>
        </div>
        <?php foreach($array_chunk_options as $options) { ?>
        <div id="product">

          <div class="container_title_product_new2">
            <?php $image_option = ''; ?>
            <?php foreach ($options as $option) { ?>

            <?php if ($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') { ?>

            <?php foreach ($option['product_option_value'] as $option_value) { ?>
            <?php if($image_option != $option_value['image']){ ?>
            <?php $image_option = $option_value['image']; ?>
            <img class="product_new_image_option" src="<?php echo $image_option; ?>" />
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php } ?>
          </div>
          <div class="container_table_general float-left">
            <div class="container_table float-left">
              <table class="table_style_product_new">
                <tr class="text_table_product_new2">
                  <td class="text_table_product_new">Форма</td>
                  <td class="text_table_product_new">Тип</td>
                  <td class="text_table_product_new">Артикул</td>
                </tr>
                <?php foreach ($additions as $addition) { ?>
                <tr class="heidch_tr">
                  <td class="text_table_product_new4">
                    <?php echo $addition['addition_form']; ?>
                  </td>
                  <td class="text_table_product_new4">
                    <?php echo $addition['addition_type']; ?>
                  </td>
                  <td class="text_table_product_new4">
                    <?php echo $addition['addition_article']; ?>
                  </td>
                </tr>
                <?php } ?>
                <tr class="tr_weight">
                  <td class="td_weight" colspan="3">Длина рабочей части</td>
                </tr>
                <tr class="tr_weight">
                  <td class="td_price" colspan="3">Цена за 1 шт.</td>
                </tr>
              </table>
            </div>
            <div class="container_table_product-type">
              <div class="text-diameter">Диаметр</div>
              <?php foreach ($options as $option) { ?>
              <?php if ($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td' ) { ?>

              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option">
                  <table>
                    <thead>
                      <tr>
                        <?php if (!empty($option['owq_has_image'])) { ?>
                        <td class="hidden">Изображения</td>
                        <?php } ?>
                        <td style="height: 27px;">
                          <?php echo $option['name']; ?>
                        </td>

                        <?php if (!empty($option['owq_has_sku'])) { ?>
                        <td class="hidden">Артикул:</td>
                        <?php } ?>
                        <?php if (!empty($option['owq_has_stock'])) { ?>
                        <td class="hidden">Наличие:</td>
                        <?php } ?>
                        <td class="hidden">Цена:</td>
                        <?php if (!empty($option['owq_discounts'])) { ?>
                        <?php foreach ($option['owq_discounts'] as $dvalue) { ?>
                        <td>Цена от<br />
                          <?php echo $dvalue; ?>
                        </td>
                        <?php } ?>
                        <?php } ?>
                        <td class="col-quantity hidden">
                          <?php echo $entry_qty; ?>:</td>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                      <tr>
                        <?php if (!empty($option['owq_has_image'])) { ?>
                        <td class="hidden">
                          <?php if (!empty($option_value['image'])) { ?><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name']; ?>" class="img-thumbnail"
                          />
                          <?php } ?>
                        </td>
                        <?php } ?>
                        <td class="hidden">
                          <input style="display:none;" type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="" data-split="/"
                            data-price="0" data-prefix="=" data-fprice="<?php echo $option_value['owq_full_price']; ?>" data-id="<?php echo $option_value['product_option_value_id']; ?>"
                          />
                          <?php echo $option_value['name']; ?>
                        </td>
                        <?php if (!empty($option['owq_has_sku'])) { ?>
                        <td class="hidden">
                          <?php echo $option_value['owq_sku']; ?>
                        </td>
                        <?php } ?>
                        <?php if (!empty($option['owq_has_stock'])) { ?>
                        <td class="stock hidden">
                          <?php if (!empty($option_value['owq_has_stock'])) { ?>
                          <?php echo $option_value['owq_quantity']; ?> шт.
                          <?php } ?>
                        </td>
                        <?php } ?>
                        <td class="hidden">
                          <?php if ($option_value['owq_full_price_text']) { ?>
                          <?php echo $option_value['owq_full_price_text']; ?>
                          <?php } ?>
                        </td>
                        <?php if (!empty($option_value['owq_discounts'])) { ?>
                        <?php foreach ($option_value['owq_discounts'] as $dvalue) { ?>
                        <td class="hidden">
                          <?php echo $dvalue; ?>
                        </td>
                        <?php } ?>
                        <?php } ?>
                        <td class="col-quantity">
                          <div class="owq-quantity"><span class="owq-sub"></span><input style="background-image: url(<?php echo $option_value['image_option']; ?>); background-position: center; border: 1px solid #12465d; border-radius: 5px; color: #fff;"
                              type="text" value="0" <?php if (!empty($option_value[ 'owq_has_stock'])) { ?>data-max="
                            <?php echo $option_value['owq_quantity']; ?>"
                            <?php } ?> class="form-control owq-input" /><span class="owq-add"></span></div>
                        </td>
                      </tr>
                      <?php } ?>
                      <?php $length_option = ''; ?>
                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                      <?php if($length_option != $option_value['length_option']){ ?>
                      <?php $length_option = $option_value['length_option']; ?>
                      <tr>
                        <td class="text_length_option">
                          <?php echo $length_option; ?>
                        </td>
                      </tr>
                      <?php } ?>
                      <?php } ?>
                      <?php $price_option = ''; ?>
                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                      <?php if($price_option != $option_value['price']){ ?>
                      <?php $price_option = $option_value['price']; ?>
                      <tr>
                        <td class="td_text_price_option">
                          <?php echo $price_option; ?>
                          </br><span class="span_text_price_option">руб</span></td>
                      </tr>
                      <?php } ?>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
              <?php $entry_qty = ''; ?>
              <?php } ?>
              <?php } ?>
            </div>
          </div>

        </div>




        <?php } ?>
        <div class="legend">
          <?php if ($attribute_groups) { ?>
          <?php foreach ($attribute_groups as $attribute_group) { ?>
          <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
          <div class="title_legend">
            <?php echo $attribute['text']; ?>
          </div>
          <?php } ?>
          <?php } ?>
          <?php } ?>
          <?php foreach ($additions as $addition) { ?>
          <div class="container_data_legend">
            <img class="image_legend" src="<?php echo $addition['addition_image']; ?>" alt="">
            <div class="name_attribute_legend">
              <?php echo $addition['addition_name']; ?>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>

      <div class="container_price_and_button-cart">
        <div class="tovar_new_price">
          <div class="price_results">Цена итого:</div>
          <span class="price_results_product_new"><?php echo $price; ?>&nbsp;руб</span>
        </div>
        <a id="product">
          <input type="hidden" name="quantity" value="<?php echo $minimum; ?>" size="2"
                 id="input-quantity" class="form-control"/>
          <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
          <button type="button" class="button_add_to_cart_product_nev" id="button-cart">
            <a class="a_add_to_cart_product_nev">Добавить в корзину</a>
        </button>
        </a>
      </div>
    </div>
    <div style="clear: both"></div>
    <div class="SEO_text_product_new">
      <?php echo $content_bottom; ?>
    </div>
  </div>
</div>



<?php } else if($options && $type_product1){ ?>

<!-- Mob_start -->
<div class="container_product_new_mob">
  <div class="container_title_product_new">
    <?php echo $heading_title; ?>
  </div>
  <div class="container_title_product_new1">
    <a href="" title="<?php echo $heading_title; ?>"><img class="thumbnail_product_new"
                                                                          src="<?php echo $thumb; ?>"
                                                                          title="<?php echo $heading_title; ?>"
                                                                          alt="<?php echo $heading_title; ?>"/></a>
  </div>
  <div id="product">
    <?php
                                $result_count = ceil($count_options);
                                $count_item_option_product = 9;
                          ?>

      <div class="container_title_product_new2 hidden">

        <?php $image_option = ''; ?>
        <?php foreach ($options as $option) { ?>
        <?php if ($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') { ?>
        <?php foreach ($option['product_option_value'] as $option_value) { ?>
        <?php if($image_option != $option_value['image']){ ?>
        <?php $image_option = $option_value['image']; ?>
        <img class="product_new_image_option" src="<?php echo $image_option; ?>" />
        <?php } ?>
        <?php } ?>
        <?php } ?>
        <?php } ?>
      </div>
      <?php $max_width = 100; ?>
      <?php $result_option_sort_order = ''; ?>
      <?php $max_option_sort_order = max(array($option_value['option_sort_order'])); ?>
      <?php $result_option_sort_order = $max_width / $max_option_sort_order; ?>

      <div class="">
        <div class="">
          <table class="table_style_product_new_mob">
            <tr class="text_table_product_new2">
              <td class="text_table_product_new_mob mob_title_td_style" colspan="6">Форма</td>
            </tr>
            <tr class="heidch_tr heidch_tr_mob">
              <?php foreach ($addition_kinds as $addition_kind) { ?>
              <td class="text_table_product_new4" style="width: <?php echo $result_option_sort_order ; ?>%">
                <?php echo $addition_kind['addition_form_kind']; ?>
              </td>
              <?php } ?>
            </tr>
            <!-- <tr>
                            <td class="text_table_product_new_mob mob_title_td_style" colspan="6">Тип</td>
                          </tr>
                          <tr class="heidch_tr heidch_tr_mob">
                            <?php foreach ($additions as $addition) { ?>
                            <td class="text_table_product_new4">
                              <?php echo $addition['addition_type']; ?>
                            </td>
                            <?php } ?>
                          </tr> -->
            <tr class="text_table_product_new2">
              <td class="text_table_product_new_mob mob_title_td_style" colspan="6">Диаметр</td>
            </tr>
          </table>
          <tr>
            <div class="container_mob_table">
              <?php foreach ($options as $option) { ?>
              <?php if ($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td' ) { ?>
              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option">
                  <table>
                    <thead>
                      <tr>
                        <?php if (!empty($option['owq_has_image'])) { ?>
                        <td class="hidden">Изображения</td>
                        <?php } ?>
                        <td class="td_style_hidden<?php echo $option['product_option_id']; ?> option_name_product_new" colspan="2"><span><?php echo $option['name']; ?></span></td>
                        <?php if (!empty($option['owq_has_sku'])) { ?>
                        <td class="hidden">Артикул:</td>
                        <?php } ?>
                        <?php if (!empty($option['owq_has_stock'])) { ?>
                        <td class="hidden">Наличие:</td>
                        <?php } ?>
                        <td class="hidden">Цена:</td>
                        <?php if (!empty($option['owq_discounts'])) { ?>
                        <?php foreach ($option['owq_discounts'] as $dvalue) { ?>
                        <td>Цена от<br />
                          <?php echo $dvalue; ?>
                        </td>
                        <?php } ?>
                        <?php } ?>
                        <td class="col-quantity hidden">
                          <?php echo $entry_qty; ?>:</td>
                      </tr>
                    </thead>
                    <tbody class="tbody_hidden<?php echo $option['product_option_id']; ?> hidden">
                      <script>
                        $(document).ready(function () {
                          $(".td_style_hidden<?php echo $option['product_option_id']; ?>").click(function () {
                            $(".tbody_hidden<?php echo $option['product_option_id']; ?>").slideToggle("slow");
                          });
                        });
                      </script>
                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                      <tr class="float-left" style="width: <?php echo $result_option_sort_order ; ?>%">
                        <?php if (!empty($option['owq_has_image'])) { ?>
                        <td class="hidden">
                          <?php if (!empty($option_value['image'])) { ?><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name']; ?>" class="img-thumbnail"
                          />
                          <?php } ?>
                        </td>
                        <?php } ?>
                        <td class="hidden">
                          <input style="display:none;" type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="" data-split="/"
                            data-price="0" data-prefix="=" data-fprice="<?php echo $option_value['owq_full_price']; ?>" data-id="<?php echo $option_value['product_option_value_id']; ?>"
                          />
                          <?php echo $option_value['name']; ?>
                        </td>
                        <?php if (!empty($option['owq_has_sku'])) { ?>
                        <td class="hidden">
                          <?php echo $option_value['owq_sku']; ?>
                        </td>
                        <?php } ?>
                        <?php if (!empty($option['owq_has_stock'])) { ?>
                        <td class="stock hidden">
                          <?php if (!empty($option_value['owq_has_stock'])) { ?>
                          <?php echo $option_value['owq_quantity']; ?> шт.
                          <?php } ?>
                        </td>
                        <?php } ?>
                        <td class="hidden">
                          <?php if ($option_value['owq_full_price_text']) { ?>
                          <?php echo $option_value['owq_full_price_text']; ?>
                          <?php } ?>
                        </td>
                        <?php if (!empty($option_value['owq_discounts'])) { ?>
                        <?php foreach ($option_value['owq_discounts'] as $dvalue) { ?>
                        <td class="hidden">
                          <?php echo $dvalue; ?>
                        </td>
                        <?php } ?>
                        <?php } ?>

                        <td class="col-quantity">
                          <div class="owq-quantity mob">
                            <div class="div_product_form_new">
                              <?php echo $option_value['type_option']; ?>
                            </div>
                            <span class="owq-sub owq-sub_mob button-minus_product_form_new"></span>
                            <input type="text" value="0" <?php if (!empty($option_value[ 'owq_has_stock'])) { ?>data-max="
                            <?php echo $option_value['owq_quantity']; ?>"
                            <?php } ?> class="form-control owq-input input_product_form_new" />
                            <span class="owq-add owq-add_mob button-plus_product_form_new"></span>
                          </div>
                        </td>

                      </tr>
                      <?php } ?>
                      <?php $length_option = ''; ?>
                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                      <?php if($length_option != $option_value['length_option']){ ?>
                      <?php $length_option = $option_value['length_option']; ?>
                      <td class="text_length_option" style="width=" 50% "">
                        <span class="text_mob_length">Длина рабочей части</span></br>
                        <span class="text_mob_length_option"><?php echo $length_option; ?></span>
                      </td>
                      <?php } ?>
                      <?php } ?>
                      <?php $price_option = ''; ?>
                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                      <?php if($price_option != $option_value['price']){ ?>
                      <?php $price_option = $option_value['price']; ?>
                      <td class="text_length_option" style="width=" 50% "">
                        <span class="text_mob_length">Цена за 1 шт.</span></br>
                        <span class="text_mob_price_option"><?php echo $price_option; ?><span class="text_mob_price_option"> руб</span></span>
                      </td>
                      <?php } ?>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
              <?php $entry_qty = ''; ?>
              <?php } ?>
              <?php } ?>
            </div>
          </tr>


          </table>

        </div>

      </div>
      <div class="legend_addition_kind">
        <?php if ($attribute_groups) { ?>
        <?php foreach ($attribute_groups as $attribute_group) { ?>
        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
        <div class="title_legend title_legend_kind">
          <?php echo $attribute['text']; ?>
        </div>
        <?php } ?>
        <?php } ?>
        <?php } ?>
        <?php foreach ($addition_kinds as $addition_kind) { ?>
        <table class="table_sort_order_addition_kind float-left">
          <tr>
            <td class="td_sort_order_addition_kind">
              <?php echo $addition_kind['sort_order_addition_kind']; ?>.</td>
          </tr>
        </table>
        <table class="table_dani_kind">
          <tr>
            <td class="td_addition_image_kind"><img class="img_addition" src="<?php echo $addition_kind['addition_image_kind']; ?>" alt="">
              <span class="span_addition_type_kind"><?php echo $addition_kind['addition_type_kind']; ?></span>
            </td>
            <td class="text_addition_length_kind">
              <?php echo $addition_kind['addition_length_kind']; ?>
            </td>
            <td class="text_addition_iso_kind">
              <?php echo $addition_kind['addition_iso_kind']; ?>
            </td>
          </tr>
        </table>
        <?php } ?>
        <div class="length_container">Длина</div>
        <div class="iso_container">ISO</div>
      </div>
      <div class="container_addition_name_kind">
        <?php foreach ($addition_kinds as $addition_kind) { ?>
        <div class="span_addition_name_kind">
          <?php echo $addition_kind['sort_order_addition_kind']; ?>.
          <?php echo $addition_kind['addition_name_kind']; ?>
        </div>
        <?php } ?>
      </div>
  </div>
</div>
<!-- Mob_end -->

<div class="container_product_new">
  <div class="container_title_product_new">
    <?php echo $heading_title; ?>
  </div>
  <div class="container_title_product_new1">
    <a href="" title="<?php echo $heading_title; ?>"><img class="thumbnail_product_new"
                                                                src="<?php echo $thumb; ?>"
                                                                title="<?php echo $heading_title; ?>"
                                                                alt="<?php echo $heading_title; ?>"/></a>
  </div>
  <?php foreach($array_chunk_options as $options) { ?>
  <div id="product">

    <div class="container_title_product_new2">
      <?php $image_option = ''; ?>
      <?php foreach ($options as $option) { ?>
      <?php if ($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') { ?>
      <?php foreach ($option['product_option_value'] as $option_value) { ?>
      <?php if($image_option != $option_value['image']){ ?>
      <?php $image_option = $option_value['image']; ?>
      <img class="product_new_image_option" src="<?php echo $image_option; ?>" />
      <?php } ?>
      <?php } ?>
      <?php } ?>
      <?php } ?>
    </div>
    <div class="container_table_general float-left" >
      <div class="container_table float-left">
        <table class="table_style_product_new" style="width: 100%;     margin-left: 1px;">
          <tr class="text_table_product_new2">
            <td class="text_table_product_new width_for_product_new">Форма</td>
            <!--   <td class="text_table_product_new">Тип</td> -->
            <td class="text_table_product_new">Артикул</td>
          </tr>
          <?php foreach ($addition_kinds as $addition_kind) { ?>
          <tr class="heidch_tr">
            <td class="text_table_product_new4">
              <?php echo $addition_kind['addition_form_kind']; ?>
            </td>
            <!-- <td class="text_table_product_new4">
                          <?php echo $addition['addition_type']; ?>
                        </td> -->
            <td class="text_table_product_new4">
              <?php echo $addition_kind['addition_article_kind']; ?>
            </td>
          </tr>
          <?php } ?>
          <tr class="tr_weight">
            <td class="td_weight" colspan="3">Длина рабочей части</td>
          </tr>
          <tr class="tr_weight">
            <td class="td_price" colspan="3">Цена за 1 шт.</td>
          </tr>
        </table>
      </div>
      <div class="container_table_product-type">
        <div class="text-diameter">Диаметр</div>
        <?php foreach ($options as $option) { ?>
        <?php if ($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td' ) { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option">
            <table>
              <thead>
                <tr>
                  <?php if (!empty($option['owq_has_image'])) { ?>
                  <td class="hidden">Изображения</td>
                  <?php } ?>
                  <td style="height: 27px;">
                    <?php echo $option['name']; ?>
                  </td>
                  <?php if (!empty($option['owq_has_sku'])) { ?>
                  <td class="hidden">Артикул:</td>
                  <?php } ?>
                  <?php if (!empty($option['owq_has_stock'])) { ?>
                  <td class="hidden">Наличие:</td>
                  <?php } ?>
                  <td class="hidden">Цена:</td>
                  <?php if (!empty($option['owq_discounts'])) { ?>
                  <?php foreach ($option['owq_discounts'] as $dvalue) { ?>
                  <td>Цена от<br />
                    <?php echo $dvalue; ?>
                  </td>
                  <?php } ?>
                  <?php } ?>
                  <td class="col-quantity hidden">
                    <?php echo $entry_qty; ?>:</td>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <tr>
                  <?php if (!empty($option['owq_has_image'])) { ?>
                  <td class="hidden">
                    <?php if (!empty($option_value['image'])) { ?><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name']; ?>" class="img-thumbnail"
                    />
                    <?php } ?>
                  </td>
                  <?php } ?>
                  <td class="hidden">
                    <input style="display:none;" type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="" data-split="/"
                      data-price="0" data-prefix="=" data-fprice="<?php echo $option_value['owq_full_price']; ?>" data-id="<?php echo $option_value['product_option_value_id']; ?>"
                    />
                    <?php echo $option_value['name']; ?>
                  </td>
                  <?php if (!empty($option['owq_has_sku'])) { ?>
                  <td class="hidden">
                    <?php echo $option_value['owq_sku']; ?>
                  </td>
                  <?php } ?>
                  <?php if (!empty($option['owq_has_stock'])) { ?>
                  <td class="stock hidden">
                    <?php if (!empty($option_value['owq_has_stock'])) { ?>
                    <?php echo $option_value['owq_quantity']; ?> шт.
                    <?php } ?>
                  </td>
                  <?php } ?>
                  <td class="hidden">
                    <?php if ($option_value['owq_full_price_text']) { ?>
                    <?php echo $option_value['owq_full_price_text']; ?>
                    <?php } ?>
                  </td>
                  <?php if (!empty($option_value['owq_discounts'])) { ?>
                  <?php foreach ($option_value['owq_discounts'] as $dvalue) { ?>
                  <td class="hidden">
                    <?php echo $dvalue; ?>
                  </td>
                  <?php } ?>
                  <?php } ?>
                  <td class="">
                    <div class="owq-quantity">
                      <div class="div_product_form_new">
                        <?php echo $option_value['type_option']; ?>
                      </div>
                      <span class="owq-sub button-minus_product_form_new"></span>
                      <input type="text" value="0" <?php if (!empty($option_value[ 'owq_has_stock'])) { ?>data-max="
                      <?php echo $option_value['owq_quantity']; ?>"
                      <?php } ?> class="form-control owq-input input_product_form_new" />
                      <span class="owq-add button-plus_product_form_new"></span>
                    </div>
                  </td>
                </tr>
                <?php } ?>
                <?php $length_option = ''; ?>
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <?php if($length_option != $option_value['length_option']){ ?>
                <?php $length_option = $option_value['length_option']; ?>
                <tr>
                  <td class="text_length_option">
                    <?php echo $length_option; ?>
                  </td>
                </tr>
                <?php } ?>
                <?php } ?>
                <?php $price_option = ''; ?>
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <?php if($price_option != $option_value['price']){ ?>
                <?php $price_option = $option_value['price']; ?>
                <tr>
                  <td class="td_text_price_option">
                    <?php echo $price_option; ?>
                    </br><span class="span_text_price_option">руб</span></td>
                </tr>
                <?php } ?>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        <?php $entry_qty = ''; ?>
        <?php } ?>
        <?php } ?>
      </div>
    </div>
  </div>
  <?php } ?>
  <div class="legend_addition_kind">
    <?php if ($attribute_groups) { ?>
    <?php foreach ($attribute_groups as $attribute_group) { ?>
    <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
    <div class="title_legend">
      <?php echo $attribute['text']; ?>
    </div>
    <?php } ?>
    <?php } ?>
    <?php } ?>
    <?php foreach ($addition_kinds as $addition_kind) { ?>
    <table class="table_sort_order_addition_kind float-left">
      <tr>
        <td class="td_sort_order_addition_kind">
          <?php echo $addition_kind['sort_order_addition_kind']; ?>.</td>
      </tr>
    </table>
    <table class="table_dani_kind">
      <tr>
        <td class="td_addition_image_kind"><img class="img_addition" src="<?php echo $addition_kind['addition_image_kind']; ?>" alt="">
          <span class="span_addition_type_kind"><?php echo $addition_kind['addition_type_kind']; ?></span>
        </td>
        <td class="text_addition_length_kind">
          <?php echo $addition_kind['addition_length_kind']; ?>
        </td>
        <td class="text_addition_iso_kind">
          <?php echo $addition_kind['addition_iso_kind']; ?>
        </td>
      </tr>
    </table>
    <?php } ?>
    <div class="container_addition_name_kind">
      <?php foreach ($addition_kinds as $addition_kind) { ?>
      <span class="span_addition_name_kind float-left"><?php echo $addition_kind['addition_name_kind']; ?></span>
      <?php } ?>
    </div>
    <div class="length_container">Длина</div>
    <div class="iso_container">ISO</div>
  </div>
</div>


<div class="container_price_and_button-cart">
  <div class="tovar_new_price">
    <div class="price_results">Цена итого:</div>
    <span class="price_results_product_new"><?php echo $price; ?>&nbsp;руб</span>
  </div>
  <a id="product">
                          <input type="hidden" name="quantity" value="<?php echo $minimum; ?>" size="2"
                                 id="input-quantity" class="form-control"/>
                          <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
                          <button type="button" class="button_add_to_cart_product_nev" id="button-cart">
                            <a class="a_add_to_cart_product_nev">Добавить в корзину</a>
  </button>
  </a>
</div>


</div>
<div class="SEO_text_product_new" style="clear: both">
  <?php echo $content_bottom; ?>
</div>
</div>
<?php } else { ?>

<!-- Mob_start -->
<div class="container_product_new_mob">
  <div class="container_title_product_new">
    <?php echo $heading_title; ?>
  </div>
  <div class="container_title_product_new1">
    <a href="" title="<?php echo $heading_title; ?>"><img class="thumbnail_product_new"
                                                                        src="<?php echo $thumb; ?>"
                                                                        title="<?php echo $heading_title; ?>"
                                                                        alt="<?php echo $heading_title; ?>"/></a>
  </div>
  <div id="product">
    <?php
                                              $result_count = ceil($count_options);
                                              $count_item_option_product = 9;
                                        ?>

      <div class="container_title_product_new2 hidden">

        <?php $image_option = ''; ?>
        <?php foreach ($options as $option) { ?>
        <?php if ($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') { ?>
        <?php foreach ($option['product_option_value'] as $option_value) { ?>
        <?php if($image_option != $option_value['image']){ ?>
        <?php $image_option = $option_value['image']; ?>
        <img class="product_new_image_option" src="<?php echo $image_option; ?>" />
        <?php } ?>
        <?php } ?>
        <?php } ?>
        <?php } ?>
      </div>
      <?php $max_width = 100; ?>
      <?php $result_option_sort_order = ''; ?>
      <?php $max_option_sort_order = max(array($option_value['option_sort_order'])); ?>
      <?php $result_option_sort_order = $max_width / $max_option_sort_order; ?>

      <div class="">
        <div class="">
          <table class="table_style_product_new_mob">
            <tr class="text_table_product_new2">
              <td class="text_table_product_new_mob mob_title_td_style" colspan="6">Форма</td>
            </tr>
            <tr class="heidch_tr heidch_tr_mob">
              <?php foreach ($addition_kinds as $addition_kind) { ?>
              <td class="text_table_product_new4" style="width: <?php echo $result_option_sort_order ; ?>%">
                <?php echo $addition_kind['addition_form_kind']; ?>
              </td>
              <?php } ?>
            </tr>
            <!-- <tr>
                          <td class="text_table_product_new_mob mob_title_td_style" colspan="6">Тип</td>
                        </tr>
                        <tr class="heidch_tr heidch_tr_mob">
                          <?php foreach ($additions as $addition) { ?>
                          <td class="text_table_product_new4">
                            <?php echo $addition['addition_type']; ?>
                          </td>
                          <?php } ?>
                        </tr> -->
            <tr class="text_table_product_new2">
              <td class="text_table_product_new_mob mob_title_td_style" colspan="6">Диаметр</td>
            </tr>
          </table>
          <tr>
            <div class="container_mob_table">
              <?php foreach ($options as $option) { ?>
              <?php if ($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td' ) { ?>
              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option">
                  <table>
                    <thead>
                      <tr>
                        <?php if (!empty($option['owq_has_image'])) { ?>
                        <td class="hidden">Изображения</td>
                        <?php } ?>
                        <td class="td_style_hidden<?php echo $option['product_option_id']; ?> option_name_product_new" colspan="2"><span><?php echo $option['name']; ?></span></td>
                        <?php if (!empty($option['owq_has_sku'])) { ?>
                        <td class="hidden">Артикул:</td>
                        <?php } ?>
                        <?php if (!empty($option['owq_has_stock'])) { ?>
                        <td class="hidden">Наличие:</td>
                        <?php } ?>
                        <td class="hidden">Цена:</td>
                        <?php if (!empty($option['owq_discounts'])) { ?>
                        <?php foreach ($option['owq_discounts'] as $dvalue) { ?>
                        <td>Цена от<br />
                          <?php echo $dvalue; ?>
                        </td>
                        <?php } ?>
                        <?php } ?>
                        <td class="col-quantity hidden">
                          <?php echo $entry_qty; ?>:</td>
                      </tr>
                    </thead>
                    <tbody class="tbody_hidden<?php echo $option['product_option_id']; ?> hidden">
                      <script>
                        $(document).ready(function () {
                          $(".td_style_hidden<?php echo $option['product_option_id']; ?>").click(function () {
                            $(".tbody_hidden<?php echo $option['product_option_id']; ?>").slideToggle("slow");
                          });
                        });
                      </script>
                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                      <tr class="float-left" style="width: <?php echo $result_option_sort_order ; ?>%">
                        <?php if (!empty($option['owq_has_image'])) { ?>
                        <td class="hidden">
                          <?php if (!empty($option_value['image'])) { ?><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name']; ?>" class="img-thumbnail"
                          />
                          <?php } ?>
                        </td>
                        <?php } ?>
                        <td class="hidden">
                          <input style="display:none;" type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="" data-split="/"
                            data-price="0" data-prefix="=" data-fprice="<?php echo $option_value['owq_full_price']; ?>" data-id="<?php echo $option_value['product_option_value_id']; ?>"
                          />
                          <?php echo $option_value['name']; ?>
                        </td>
                        <?php if (!empty($option['owq_has_sku'])) { ?>
                        <td class="hidden">
                          <?php echo $option_value['owq_sku']; ?>
                        </td>
                        <?php } ?>
                        <?php if (!empty($option['owq_has_stock'])) { ?>
                        <td class="stock hidden">
                          <?php if (!empty($option_value['owq_has_stock'])) { ?>
                          <?php echo $option_value['owq_quantity']; ?> шт.
                          <?php } ?>
                        </td>
                        <?php } ?>
                        <td class="hidden">
                          <?php if ($option_value['owq_full_price_text']) { ?>
                          <?php echo $option_value['owq_full_price_text']; ?>
                          <?php } ?>
                        </td>
                        <?php if (!empty($option_value['owq_discounts'])) { ?>
                        <?php foreach ($option_value['owq_discounts'] as $dvalue) { ?>
                        <td class="hidden">
                          <?php echo $dvalue; ?>
                        </td>
                        <?php } ?>
                        <?php } ?>

                        <td class="col-quantity">
                          <div class="owq-quantity mob"><span class="owq-sub"></span><input style="background-image: url(<?php echo $option_value['image_option']; ?>); background-position: center; border: 1px solid #12465d; border-radius: 5px; color: #000;"
                              type="text" value="0" <?php if (!empty($option_value[ 'owq_has_stock'])) { ?>data-max="
                            <?php echo $option_value['owq_quantity']; ?>"
                            <?php } ?> class="form-control owq-input" /><span class="owq-add"></span></div>
                        </td>

                      </tr>
                      <?php } ?>
                      <?php $length_option = ''; ?>
                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                      <?php if($length_option != $option_value['length_option']){ ?>
                      <?php $length_option = $option_value['length_option']; ?>
                      <td class="text_length_option" style="width=" 50% "">
                        <span class="text_mob_length">Длина рабочей части</span></br>
                        <span class="text_mob_length_option"><?php echo $length_option; ?></span>
                      </td>
                      <?php } ?>
                      <?php } ?>
                      <?php $price_option = ''; ?>
                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                      <?php if($price_option != $option_value['price']){ ?>
                      <?php $price_option = $option_value['price']; ?>
                      <td class="text_length_option" style="width=" 50% "">
                        <span class="text_mob_length">Цена за 1 шт.</span></br>
                        <span class="text_mob_price_option"><?php echo $price_option; ?><span class="text_mob_price_option"> руб</span></span>
                      </td>
                      <?php } ?>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
              <?php $entry_qty = ''; ?>
              <?php } ?>
              <?php } ?>
            </div>
          </tr>


          </table>
        </div>
      </div>
  </div>
</div>
<!-- Mob_end -->

<div class="container_product_new">
  <div class="container_title_product_new">
    <?php echo $heading_title; ?>
  </div>
  <div class="container_title_product_new1">
    <a href="" title="<?php echo $heading_title; ?>"><img class="thumbnail_product_new"
                                                                src="<?php echo $thumb; ?>"
                                                                title="<?php echo $heading_title; ?>"
                                                                alt="<?php echo $heading_title; ?>"/></a>
  </div>
  <?php foreach($array_chunk_options as $options) { ?>
  <div id="product">

    <div class="container_title_product_new2">
      <?php $image_option = ''; ?>
      <?php foreach ($options as $option) { ?>
      <?php if ($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') { ?>
      <?php foreach ($option['product_option_value'] as $option_value) { ?>
      <?php if($image_option != $option_value['image']){ ?>
      <?php $image_option = $option_value['image']; ?>
      <img class="product_new_image_option" src="<?php echo $image_option; ?>" />
      <?php } ?>
      <?php } ?>
      <?php } ?>
      <?php } ?>
    </div>
    <div class="container_table_general float-left">
      <div class="container_table float-left">
        <table class="table_style_product_new" style="width: 100%;     margin-left: 1px;">
          <tr class="text_table_product_new2">
            <td class="text_table_product_new width_for_product_new">Форма</td>
            <!--   <td class="text_table_product_new">Тип</td> -->
            <td class="text_table_product_new">Артикул</td>
          </tr>
          <?php foreach ($addition_kinds as $addition_kind) { ?>
          <tr class="heidch_tr">
            <td class="text_table_product_new4">
              <?php echo $addition_kind['addition_form_kind']; ?>
            </td>
            <!-- <td class="text_table_product_new4">
                             <?php echo $addition['addition_type']; ?>
                           </td> -->
            <td class="text_table_product_new4">
              <?php echo $addition_kind['addition_article_kind']; ?>
            </td>
          </tr>
          <?php } ?>
          <tr class="tr_weight">
            <td class="td_weight" colspan="3">Длина рабочей части</td>
          </tr>
          <tr class="tr_weight">
            <td class="td_price" colspan="3">Цена за 1 шт.</td>
          </tr>
        </table>
      </div>
      <div class="container_table_product-type">
        <div class="text-diameter">Диаметр</div>
        <?php foreach ($options as $option) { ?>
        <?php if ($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td' ) { ?>
        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
          <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option">
            <table>
              <thead>
                <tr>
                  <?php if (!empty($option['owq_has_image'])) { ?>
                  <td class="hidden">Изображения</td>
                  <?php } ?>
                  <td style="height: 27px;">
                    <?php echo $option['name']; ?>
                  </td>
                  <?php if (!empty($option['owq_has_sku'])) { ?>
                  <td class="hidden">Артикул:</td>
                  <?php } ?>
                  <?php if (!empty($option['owq_has_stock'])) { ?>
                  <td class="hidden">Наличие:</td>
                  <?php } ?>
                  <td class="hidden">Цена:</td>
                  <?php if (!empty($option['owq_discounts'])) { ?>
                  <?php foreach ($option['owq_discounts'] as $dvalue) { ?>
                  <td>Цена от<br />
                    <?php echo $dvalue; ?>
                  </td>
                  <?php } ?>
                  <?php } ?>
                  <td class="col-quantity hidden">
                    <?php echo $entry_qty; ?>:</td>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <tr>
                  <?php if (!empty($option['owq_has_image'])) { ?>
                  <td class="hidden">
                    <?php if (!empty($option_value['image'])) { ?><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name']; ?>" class="img-thumbnail"
                    />
                    <?php } ?>
                  </td>
                  <?php } ?>
                  <td class="hidden">
                    <input style="display:none;" type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="" data-split="/"
                      data-price="0" data-prefix="=" data-fprice="<?php echo $option_value['owq_full_price']; ?>" data-id="<?php echo $option_value['product_option_value_id']; ?>"
                    />
                    <?php echo $option_value['name']; ?>
                  </td>
                  <?php if (!empty($option['owq_has_sku'])) { ?>
                  <td class="hidden">
                    <?php echo $option_value['owq_sku']; ?>
                  </td>
                  <?php } ?>
                  <?php if (!empty($option['owq_has_stock'])) { ?>
                  <td class="stock hidden">
                    <?php if (!empty($option_value['owq_has_stock'])) { ?>
                    <?php echo $option_value['owq_quantity']; ?> шт.
                    <?php } ?>
                  </td>
                  <?php } ?>
                  <td class="hidden">
                    <?php if ($option_value['owq_full_price_text']) { ?>
                    <?php echo $option_value['owq_full_price_text']; ?>
                    <?php } ?>
                  </td>
                  <?php if (!empty($option_value['owq_discounts'])) { ?>
                  <?php foreach ($option_value['owq_discounts'] as $dvalue) { ?>
                  <td class="hidden">
                    <?php echo $dvalue; ?>
                  </td>
                  <?php } ?>
                  <?php } ?>
                  <td class="col-quantity">
                    <div class="owq-quantity"><span class="owq-sub"></span><input style="background-image: url(<?php echo $option_value['image_option']; ?>); background-position: center; border: 1px solid #12465d; border-radius: 5px; color: #000;"
                        type="text" value="0" <?php if (!empty($option_value[ 'owq_has_stock'])) { ?>data-max="
                      <?php echo $option_value['owq_quantity']; ?>"
                      <?php } ?> class="form-control owq-input" /><span class="owq-add"></span></div>
                  </td>
                </tr>
                <?php } ?>
                <?php $length_option = ''; ?>
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <?php if($length_option != $option_value['length_option']){ ?>
                <?php $length_option = $option_value['length_option']; ?>
                <tr>
                  <td class="text_length_option">
                    <?php echo $length_option; ?>
                  </td>
                </tr>
                <?php } ?>
                <?php } ?>
                <?php $price_option = ''; ?>
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <?php if($price_option != $option_value['price']){ ?>
                <?php $price_option = $option_value['price']; ?>
                <tr>
                  <td class="td_text_price_option">
                    <?php echo $price_option; ?>
                    </br><span class="span_text_price_option">руб</span></td>
                </tr>
                <?php } ?>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        <?php $entry_qty = ''; ?>
        <?php } ?>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<?php } ?>


<div class="container_price_and_button-cart min_margin_mob_container_cart">
  <div class="tovar_new_price">
    <div class="price_results">Цена итого:</div>
    <span class="price_results_product_new"><?php echo $price; ?>&nbsp;руб</span>
  </div>
  <a id="product">
                  <input type="hidden" name="quantity" value="<?php echo $minimum; ?>" size="2"
                         id="input-quantity" class="form-control"/>
                  <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
                  <button type="button" class="button_add_to_cart_product_nev" id="button-cart">
                    <a class="a_add_to_cart_product_nev">Добавить в корзину</a>
  </button>
  </a>
</div>
<div style="clear: both; height: 100px"></div>


</div>
<div class="SEO_text_product_new" style="clear: both">
  <?php echo $content_bottom; ?>
</div>
</div>
<?php } ?>
</div>

</div>
</div>


<script type="text/javascript"><!--
    $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
        $.ajax({
            url: 'index.php?route=product/product/getRecurringDescription',
            type: 'post',
            data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
            dataType: 'json',
            beforeSend: function() {
                $('#recurring-description').html('');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();

                if (json['success']) {
                    $('#recurring-description').html(json['success']);
                }
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('#button-cart').on('click', function() {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
            dataType: 'json',
            beforeSend: function() {
                //$('#button-cart').button('loading');
            },
            complete: function() {
               // $('#button-cart').button('reset');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    setTimeout(function () {
                        $('#cart > div').html('<span> ' + json['total'] + '</span>');
                    }, 100);
                    setTimeout(function () {
                        $('#cart > span').html('<p>Сумма:&nbsp;<span class="header-cart__price"> ' + json['total2'] + '</span>&nbsp;<span class="header-cart__cur">руб.</span></p>');
                    }, 100);
                    setTimeout(function () {
                        $('.cart_adds .count-wrap').html('<span> ' + json['total'] + '</span>');
                    }, 100);

                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('button[id^=\'button-upload\']').on('click', function() {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function() {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $(node).button('loading');
                    },
                    complete: function() {
                        $(node).button('reset');
                    },
                    success: function(json) {
                        $('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script type="text/javascript"><!--
    $('#review').delegate('.pagination a', 'click', function(e) {
        e.preventDefault();

        $('#review').fadeOut('slow');

        $('#review').load(this.href);

        $('#review').fadeIn('slow');
    });

    $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

    $('#button-review').on('click', function() {
        $.ajax({
            url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),
            beforeSend: function() {
                $('#button-review').button('loading');
            },
            complete: function() {
                $('#button-review').button('reset');
            },
            success: function(json) {
                $('.alert-success, .alert-danger').remove();

                if (json['error']) {
                    $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                }

                if (json['success']) {
                    $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').prop('checked', false);
                }
            }
        });
    });

    $(document).ready(function() {
        $('.thumbnails').magnificPopup({
            type:'image',
            delegate: 'a',
            gallery: {
                enabled:true
            }
        });
    });
    //--></script>

<script type="text/javascript"><!--
    function price_format(price)
    {
        c = <?php echo (empty($autocalc_currency['decimals']) ? "0" : $autocalc_currency['decimals'] ); ?>;
        d = '<?php echo $autocalc_currency['decimal_point']; ?>'; // decimal separator
        t = '<?php echo $autocalc_currency['thousand_point']; ?>'; // thousands separator
        s_left = '<?php echo str_replace("'", "\'", $autocalc_currency['symbol_left']); ?>';
        s_right = '<?php echo str_replace("'", "\'", $autocalc_currency['symbol_right']); ?>';
        n = price * <?php echo $autocalc_currency['value']; ?>;
        i = parseInt(n = Math.abs(n).toFixed(c)) + '';
        j = ((j = i.length) > 3) ? j % 3 : 0;
        price_text = s_left + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '') + s_right;

    <?php if (!empty($autocalc_currency2)) { ?>
        c = <?php echo (empty($autocalc_currency2['decimals']) ? "0" : $autocalc_currency2['decimals'] ); ?>;
        d = '<?php echo $autocalc_currency2['decimal_point']; ?>'; // decimal separator
        t = '<?php echo $autocalc_currency2['thousand_point']; ?>'; // thousands separator
        s_left = '<?php echo str_replace("'", "\'", $autocalc_currency2['symbol_left']); ?>';
        s_right = '<?php echo str_replace("'", "\'", $autocalc_currency2['symbol_right']); ?>';
        n = price * <?php echo $autocalc_currency2['value']; ?>;
        i = parseInt(n = Math.abs(n).toFixed(c)) + '';
        j = ((j = i.length) > 3) ? j % 3 : 0;
        price_text += '  <span class="currency2">(' + s_left + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '') + s_right + ')</span>';
    <?php } ?>

        return price_text;
    }

    function calculate_tax(price)
    {
    <?php // Process Tax Rates
        if (isset($tax_rates) && $tax) {
            foreach ($tax_rates as $tax_rate) {
                if ($tax_rate['type'] == 'F') {
                    echo 'price += '.$tax_rate['rate'].';';
                } elseif ($tax_rate['type'] == 'P') {
                    echo 'price += (price * '.$tax_rate['rate'].') / 100.0;';
                }
            }
        }
            ?>
        return price;
    }

    function process_discounts(price, quantity)
    {
    <?php
        foreach ($dicounts_unf as $discount) {
            echo 'if ((quantity >= '.$discount['quantity'].') && ('.$discount['price'].' < price)) price = '.$discount['price'].';'."\n";
    }
            ?>
        return price;
    }


    animate_delay = 20;

    main_price_final = calculate_tax(<?php echo $price_value; ?>);
    main_price_start = calculate_tax(<?php echo $price_value; ?>);
    main_step = 0;
    main_timeout_id = 0;

    function animateMainPrice_callback() {
        main_price_start += main_step;

        if ((main_step > 0) && (main_price_start > main_price_final)){
            main_price_start = main_price_final;
        } else if ((main_step < 0) && (main_price_start < main_price_final)) {
            main_price_start = main_price_final;
        } else if (main_step == 0) {
            main_price_start = main_price_final;
        }

        $('.autocalc-product-price').html( price_format(main_price_start) );

        if (main_price_start != main_price_final) {
            main_timeout_id = setTimeout(animateMainPrice_callback, animate_delay);
        }
    }

    function animateMainPrice(price) {
        main_price_start = main_price_final;
        main_price_final = price;
        main_step = (main_price_final - main_price_start) / 10;

        clearTimeout(main_timeout_id);
        main_timeout_id = setTimeout(animateMainPrice_callback, animate_delay);
    }


    <?php if ($special) { ?>
        special_price_final = calculate_tax(<?php echo $special_value; ?>);
        special_price_start = calculate_tax(<?php echo $special_value; ?>);
        special_step = 0;
        special_timeout_id = 0;

        function animateSpecialPrice_callback() {
            special_price_start += special_step;

            if ((special_step > 0) && (special_price_start > special_price_final)){
                special_price_start = special_price_final;
            } else if ((special_step < 0) && (special_price_start < special_price_final)) {
                special_price_start = special_price_final;
            } else if (special_step == 0) {
                special_price_start = special_price_final;
            }

            $('.autocalc-product-special').html( price_format(special_price_start) );

            if (special_price_start != special_price_final) {
                special_timeout_id = setTimeout(animateSpecialPrice_callback, animate_delay);
            }
        }

        function animateSpecialPrice(price) {
            special_price_start = special_price_final;
            special_price_final = price;
            special_step = (special_price_final - special_price_start) / 10;

            clearTimeout(special_timeout_id);
            special_timeout_id = setTimeout(animateSpecialPrice_callback, animate_delay);
        }
    <?php } ?>


    function recalculateprice()
    {
        var main_price = <?php echo (float)$price_value; ?>;
        var input_quantity = Number($('input[name="quantity"]').val());
        var special = <?php echo (float)$special_value; ?>;
        var tax = 0;
        discount_coefficient = 1;

        if (isNaN(input_quantity)) input_quantity = 0;

    <?php if ($special) { ?>
        special_coefficient = <?php echo ((float)$price_value/(float)$special_value); ?>;
    <?php } else { ?>
    <?php if (empty($autocalc_option_discount)) { ?>
            main_price = process_discounts(main_price, input_quantity);
            tax = process_discounts(tax, input_quantity);
        <?php } else { ?>
            if (main_price) discount_coefficient = process_discounts(main_price, input_quantity) / main_price;
        <?php } ?>
    <?php } ?>


        var option_price = 0;

    <?php if ($points) { ?>
        var points = <?php echo (float)$points_value; ?>;
        $('input:checked,option:selected').each(function() {
            if ($(this).data('points')) points += Number($(this).data('points'));
        });
        $('.autocalc-product-points').html(points);
    <?php } ?>

        $('input:checked,option:selected').each(function() {
            if ($(this).data('prefix') == '=') {
                option_price += Number($(this).data('price'));
                main_price = 0;
                special = 0;
            }
        });

        $('input:checked,option:selected').each(function() {
            if ($(this).data('prefix') == '+') {
                option_price += Number($(this).data('price'));
            }
            if ($(this).data('prefix') == '-') {
                option_price -= Number($(this).data('price'));
            }
            if ($(this).data('prefix') == 'u') {
                pcnt = 1.0 + (Number($(this).data('price')) / 100.0);
                option_price *= pcnt;
                main_price *= pcnt;
                special *= pcnt;
            }
            if ($(this).data('prefix') == 'd') {
                pcnt = 1.0 - (Number($(this).data('price')) / 100.0);
                option_price *= pcnt;
                main_price *= pcnt;
                special *= pcnt;
            }
            if ($(this).data('prefix') == '*') {
                option_price *= Number($(this).data('price'));
                main_price *= Number($(this).data('price'));
                special *= Number($(this).data('price'));
            }
            if ($(this).data('prefix') == '/') {
                option_price /= Number($(this).data('price'));
                main_price /= Number($(this).data('price'));
                special /= Number($(this).data('price'));
            }
        });

        special += option_price;
        main_price += option_price;

    <?php if ($special) { ?>
    <?php if (empty($autocalc_option_special))  { ?>
            main_price = special * special_coefficient;
        <?php } else { ?>
            special = main_price / special_coefficient;
        <?php } ?>
        tax = special;
    <?php } else { ?>
    <?php if (!empty($autocalc_option_discount)) { ?>
            main_price *= discount_coefficient;
        <?php } ?>
        tax = main_price;
    <?php } ?>

        // Process TAX.
        main_price = calculate_tax(main_price);
        special = calculate_tax(special);

    <?php if (!$autocalc_not_mul_qty) { ?>
        if (input_quantity > 0) {
            main_price *= input_quantity;
            special *= input_quantity;
            tax *= input_quantity;
        }
    <?php } ?>

        // Display Main Price
        animateMainPrice(main_price);

    <?php if ($special) { ?>
        animateSpecialPrice(special);
    <?php } ?>
    }

    $(document).ready(function() {
        $('input[type="checkbox"]').bind('change', function() { recalculateprice(); });
        $('input[type="radio"]').bind('change', function() { recalculateprice(); });
        $('select').bind('change', function() { recalculateprice(); });

        $quantity = $('input[name="quantity"]');
        $quantity.data('val', $quantity.val());
        (function() {
            if ($quantity.val() != $quantity.data('val')){
                $quantity.data('val',$quantity.val());
                recalculateprice();
            }
            setTimeout(arguments.callee, 250);
        })();

        <?php if ($autocalc_select_first) { ?>
            $('select[name^="option"] option[value=""]').remove();
            last_name = '';
            $('input[type="radio"][name^="option"]').each(function(){
                if ($(this).attr('name') != last_name) $(this).prop('checked', true);
                last_name = $(this).attr('name');
            });
        <?php } ?>

        recalculateprice();
    });

    //--></script>
<script type="text/javascript"><!--
    function update_qty_options() {
        $('.owq-option input[type="checkbox"]').each(function() {
            $qty = $(this).closest('tr').find('.owq-input');
            opt_qty = Number($qty.val());
            if (isNaN(opt_qty)) opt_qty = 0;

            if ($qty.data('max') && opt_qty > $qty.data('max')) {
                $qty.closest('tr').addClass('no-stock');
            } else {
                $qty.closest('tr').removeClass('no-stock');
            }

            if ($(this).data('id') && opt_qty > 0) {
                $(this).val($(this).data('id') + $(this).data('split') + opt_qty).data('price', $(this).data('fprice') * opt_qty).prop('checked', true);
            } else {
                $(this).prop('checked', false);
            }
        });
        $('.owq-option select').each(function() {
            $qty = $(this).closest('div').find('.owq-input');
            opt_qty = Number($qty.val());
            if (isNaN(opt_qty)) opt_qty = 0;

            $(this).find('option').each(function(){
                if ($(this).data('id') && opt_qty > 0) {
                    $(this).val($(this).data('id') + '|' + opt_qty).data('price', $(this).data('fprice') * opt_qty);
                } else {
                    $(this).val('').data('price', 0);
                }
            });
        });
    }
    $(document).ready(function(){
        $('.owq-option .owq-input').on('input',function(){
            update_qty_options();
            if(typeof recalculateprice == 'function') {
                recalculateprice();
            }
        });
        $('.owq-quantity .owq-add').on('click', function() {
            $input = $(this).prev();
            qty = Number($input.val());
            if (isNaN(qty)) qty = 0;
            qty++;
            <?php /*if ($input.data('max') && qty > $input.data('max')) qty = $input.data('max');*/ ?>
            $input.val(qty).trigger('input');
        });
        $('.owq-quantity .owq-sub').on('click', function() {
            $input = $(this).next();
            qty = Number($input.val());
            if (isNaN(qty)) qty = 0;
            qty--;
            if (qty < 1) qty = '0';
            $input.val(qty).trigger('input');
        });
        update_qty_options();
    });
    //--></script>

<?php echo $footer; ?>