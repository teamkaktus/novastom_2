<?php echo $header; ?><?php if( ! empty( $mfilter_json ) ) { echo '<div id="mfilter-json" style="display:none">' . base64_encode( $mfilter_json ) . '</div>'; } ?>
<div class="content clearfix">
<div class="container">
  <div class="breadcrumb breadcrumb--bd breadcrumb--sd">
    <ul class="breadcrumb-list">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <div class="row">
  <div class="sidebar">
  <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    </div>
    <div class="main">
    <div id="content" class="<?php echo $class; ?> padd_0_sm_9" ><?php echo $content_top; ?><div id="mfilter-content-container">
      <div class="nav-mob">
        <div id="nav-catalog" class="nav-mob__item nav-btn nav-btn--catalog">
          <div class="nav-btn__wrap nav-btn__wrap--catalog">
            <b></b>
            <b></b>
            <b></b>
          </div>
          <span>Каталог</span>
        </div>
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <div class="nav-mob__item nav-mob__item--in">
          <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        </div>
        <?php } ?>
        <div class="nav-mob__item nav-mob__item--end"><?php echo $heading_title; ?></div>
      </div>
      <div class="page-title page-title--catalog"><?php echo $heading_title; ?></div>
     
      <div class="block-catalog clearfix">
        <?php foreach ($products as $product) { ?>
        <div class="block-item block-item--catalog">
            <div class="block-item__pict block-item__pict--catalog"><a href="<?php echo $product['href']; ?>">
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
            <div>
                <div class="block-item__title block-item__title--catalog">
                  <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                </div>
               
                <?php if ($product['price']) { ?>
                <div class="price">
                  <?php if (!$product['special']) { ?>
                  <div class="block-item__price block-item__price--catalog"><?php echo $product['price']; ?><span>&nbsp;руб.</span></div>
                  <?php } else { ?>
                  <div class="block-item__price block-item__price--catalog"><?php echo $product['special']; ?><span>&nbsp;руб.</span></div>
                  <?php } ?>
                  
                </div>
                <?php } ?>
                <a class="block-item__cart block-item__cart--catalog" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">Корзина</a>
                <a class="btn btn--cart" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><?php echo $button_cart; ?></a>
                <a data-pdqo-item="<?php echo $product['product_id']; ?>" onclick="pdqoObject(this);" data-effect="mfp-zoom-in" class="btn btn--oneclick">Заказ<br>в один клик</a>
                 </div>
        </div>
        <?php } ?>
      </div>
      <div class="row">
        <div class="pagination"><?php echo $pagination; ?></div>
      </div>

      </div><?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
</div>
</div>
<?php echo $footer; ?>
