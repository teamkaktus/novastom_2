<?php echo $header; ?>
<div class="content clearfix">
    <div class="container">
        <div class="breadcrumb breadcrumb--bd">
            <ul class="breadcrumb-list">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <?php if ($breadcrumb ['href']==('')){ ?>
                    <li><?php echo $breadcrumb['text']; ?></li>
                <?php }else{ ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            <?php } ?>
            </ul>
        </div>
        <div class="navigation_panel_product_new">
             <?php foreach ($categories_ch as $category_ch) { ?>
            <?php if($heading_title == $category_ch['name']){ ?>
            <div class="navigation_panel_container_name_product_new navigation_panel_container_name_product_new_active">
                <a href="<?php echo $category_ch['href']; ?>">
                    <span class="navigation_panel_text_name_tovar_product_new"><?php echo $category_ch['name']; ?></span>
                </a>
            </div>
            <?php } else { ?>
            <div class="navigation_panel_container_name_product_new">
                <a href="<?php echo $category_ch['href']; ?>">
                    <span class="navigation_panel_text_name_tovar_product_new"><?php echo $category_ch['name']; ?></span>
                </a>
            </div>
            <?php } ?>
            <?php } ?>
        </div>
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?> padd_0_sm_9"><?php echo $content_top; ?>
                <div class="nav-mob">
                    <div id="nav-catalog" class="nav-mob__item nav-btn nav-btn--catalog nav-mob_product_new">
                        <div class="nav-btn__wrap nav-btn__wrap--catalog">
                            <b></b>
                            <b></b>
                            <b></b>
                        </div>
                        <span>Каталог</span>
                    </div>
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <div class="nav-mob__item nav-mob__item--in nav-mob__item--in_product_new ">
                        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                    </div>
                    <?php } ?>
                    <div class="nav-mob__item nav-mob__item--end"><?php echo $breadcrumb['text']; ?></div>
                </div>
                <div class="page-title page-title--catalog"><?php echo $heading_title; ?></div>
                <div class="">
                    <?php foreach ($products as $product) { ?>
                    <div class="container_catalog_product">
                        <div class="container_catalog_product_1">
                            <div class="container_image_catalog_product">
                                <a href="<?php echo $product['href']; ?>">
                                    <img class="image_catalog_product" src="<?php echo $product['image']; ?>"
                                         alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>"/>
                                </a>
                            </div>
                            <div class="container_style_catalog_product">
                                <div class="index_container_style">
                                    <span class="index_text_style"><?php echo $product['index_text']; ?></span>
                                </div>
                                <div class="icon_container_style">
                                    <?php if ($product['options']) { ?>
                                    <?php foreach ($product['options'] as $option) { ?>
                                    <?php if ($option['type'] == 'image') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <div class="radio" style="float: right">
                                                <label>
                                                    <input type="radio"
                                                           name="option[<?php echo $option['product_option_id']; ?>]"
                                                           value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                                    <img src="<?php echo $option_value['image']; ?>"
                                                         alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                                                         class="option_img_style"/>
                                                    <?php if ($option_value['price']) { ?>
                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>
                                                    )
                                                    <?php } ?>
                                                </label>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php } ?>
                                    <?php } ?>
                                </div>
                                <div class="manufacturer_container_style">
                                    <span class="manufacturer_text_style"><?php echo $product['manufacturer']; ?></span>
                                </div>
                                <div class="name_container_style">
                                    <a href="<?php echo $product['href']; ?>">
                                        <span><?php echo $product['name']; ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="container_catalog_product_2">
                                <span class="text_index_catalog_product_2">
                                    <?php echo $product['index_text']; ?>
                                </span>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="row" style="clear: both">
                    <div class="pagination"><?php echo $pagination; ?></div>
                </div>
                <div class="SEO_text_catalog_product">
                <?php echo $content_bottom; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
