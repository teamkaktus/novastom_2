<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=640, initial-scale=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
    <base href="<?php echo $base; ?>"/>
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " -
    ". ((int) $_GET['page'])." ".$text_page;} ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>"/>
    <?php } ?>
    <meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " -
    ". ((int) $_GET['page'])." ".$text_page;} ?>" />
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo $og_url; ?>"/>
    <?php if ($og_image) { ?>
    <meta property="og:image" content="<?php echo $og_image; ?>"/>
    <?php } else { ?>
    <meta property="og:image" content="<?php echo $logo; ?>"/>
    <?php } ?>
    <meta property="og:site_name" content="<?php echo $name; ?>"/>
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css"/>
    <script src="/catalog/view/javascript/libs/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>

    <script src="catalog/view/javascript/mf/jquery-ui.min.js" type="text/javascript"></script>
    <link href="catalog/view/javascript/libs/normalize/normilize.css" rel="stylesheet" type="text/css"/>
    <link href="catalog/view/theme/default/stylesheet/fonts.css" rel="stylesheet" type="text/css"/>
    <link href="catalog/view/theme/default/stylesheet/main.css" rel="stylesheet" type="text/css"/>
    <link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet" type="text/css"/>
    <link href="catalog/view/javascript/libs/magnific/magnific-popup.css" rel="stylesheet" type="text/css"/>
    <link href="catalog/view/javascript/libs/slick/slick.css" rel="stylesheet" type="text/css"/>
    <link href="catalog/view/javascript/libs/formstyler/jquery.formstyler.css" rel="stylesheet" type="text/css"/>
    

    <link href="catalog/view/javascript/Mob_menu/css/component.css" rel="stylesheet" type="text/css"/>
    
   

    <link href="catalog/view/theme/default/stylesheet/main.min.css" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.jsdelivr.net/sweetalert2/6.4.2/sweetalert2.min.css" rel="stylesheet" type="text/css"/>
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>"
          media="<?php echo $style['media']; ?>"/>
    <?php } ?>
    <script src="/catalog/view/javascript/libs/modernizr/modernizr.js" type="text/javascript"></script>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
    <?php } ?>
    <link rel="stylesheet" type="text/css"
          href="catalog/view/stylesheet/pdqo/vendor/magnific-popup/magnific-popup.min.css"/>
    <link rel="stylesheet" type="text/css" href="catalog/view/stylesheet/pdqo/vendor/animate/animate.min.css"/>
    <link rel="stylesheet" type="text/css" href="catalog/view/stylesheet/pdqo/pdqo.min.css"/>
    <link rel="stylesheet" type="text/css" href="catalog/view/stylesheet/pdqo/default.css"/>

    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
   <?php

function user_browser($agent) {
	preg_match("/(Safari|MSIE|Opera|Firefox|Chrome|Version|Opera Mini|Netscape|Konqueror|SeaMonkey|Camino|Minefield|Iceweasel|K-Meleon|Maxthon)(?:\/| )([0-9.]+)/", $agent, $browser_info); // регулярное выражение, которое позволяет отпределить 90% браузеров
        list(,$browser,$version) = $browser_info; // получаем данные из массива в переменную
        if (preg_match("/Opera ([0-9.]+)/i", $agent, $opera)) return 'Opera '.$opera[1]; // определение _очень_старых_ версий Оперы (до 8.50), при желании можно убрать
         return $browser; // для всех остальных возвращаем браузер и версию
}

?>
    <link rel="stylesheet" type="text/css" media="all"  href="catalog/view/theme/default/stylesheet/<?php

         if (user_browser($_SERVER['HTTP_USER_AGENT']) =='Safari'){
             echo "safari.css";
         }elseif(user_browser($_SERVER['HTTP_USER_AGENT']) == 'Version') {
            echo "safari.css";
         }
          else
         {
         }

   ?>" />
</head>
<body style="margin: 0;" class="<?php echo $class; ?>">

<header class="header">
    <div class="header-top clearfix">

        <div class="main-menu clearfix">
            <div id="mainmenu" class="container menu__style_header">
            <ul>
                <?php 
                $children1 = '';
                $children2 = '';
                foreach ($categories as $category_1) { 
                    if (empty($category_1['children'])){
                        $children1 = '';
                    } else {
                        $children1 = 'arrow';
                    }
                ?>
                <li class="parent <?=$children1;?>"><a
                            href="<?php echo $category_1['href'] ; ?>"><?php echo $category_1['name'] ; ?></a>
                
                <ul>
                    <?php foreach ($category_1['children'] as $category_2) { 
                        if (empty($category_2['children'])){
                            $children2 = '';
                        } else {
                            $children2 = 'arrow';
                        }
                    ?>
                    <li class="parent <?=$children2;?>"><a
                                href="<?= $category_2['href']; ?>"><?= $category_2['name']; ?></a>
                    <ul>
                        <?php foreach ($category_2['children'] as $category_3) { ?>
                        <li class="parent"><a
                                    href="<?= $category_3['href']; ?>"><?= $category_3['name']; ?></a>
                        <?php } ?>
                    </ul>
                        </li>
                    <?php } ?>
                </ul>
                    </li>
                <?php } ?>
                </li>
            </ul>
        </div>
          <div class="container">
        <div id="dl-menu" class="dl-menuwrapper menu_mob_style_header">
            <button class="button_menu_hidden">Open Menu</button>
            <ul class="dl-menu">
                     <?php 
                $children1 = '';
                $children2 = '';
                foreach ($categories as $category_1) { 
                    if (empty($category_1['children'])){
                        $children1 = '';
                    } else {
                        $children1 = 'arrow';
                    }
                ?>
                    <li><a class="<?=$children1;?>" href="<?php echo $category_1['href'] ; ?>"><?php echo $category_1['name'] ; ?></a>
                     <?php if (empty($category_1['children'])){
                        echo '<ul>';
                    } else {
                        echo '<ul class="dl-submenu">';
                    }
                    ?>
                        
                            <li class="dl-back"><a href="#">Назад</a></li>
                            <?php foreach ($category_1['children'] as $category_2) { 
                        if (empty($category_2['children'])){
                            $children2 = '';
                        } else {
                            $children2 = 'arrow';
                        }
                    ?>
                            <li>
                                <a class="<?=$children2;?>" href="<?= $category_2['href']; ?>"><?= $category_2['name']; ?></a>
                        <?php if (empty($category_2['children'])){
                            echo '<ul>';
                        } else {
                           echo '<ul class="dl-submenu">';
                        }
                        ?>
                             
                                    <li class="dl-back"><a href="#">Назад</a></li>
                                        <?php foreach ($category_2['children'] as $category_3) { ?>
                                    <li><a href="<?= $category_3['href']; ?>"><?= $category_3['name']; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php } ?>
			</ul>
        </div>
        </div>
    </div>

    <div class="container">

        <div id="nav-main" class="nav-btn">
            <div class="nav-btn__wrap">
                <b></b>
                <b></b>
                <b></b>
            </div>
            <span>Каталог</span>
        </div>
        <a href="<?php echo $home; ?>" class="logo logo--mob">
            <img class="logo__pict logo__pict--mob" src="<?php echo $logo_mob; ?>" alt="<?php echo $name; ?>">
            <div class="logo__title logo__title--mob">Novastom</div>
        </a>
        <ul class="nav-menu">
            <li class="nav-menu__item"><a href="index.php?route=information/information&information_id=4" class="nav-menu__link">О компании</a></li>
            <li class="nav-menu__item"><a href="index.php?route=information/news" class="nav-menu__link">Новости</a></li>
            <li class="nav-menu__item"><a href="index.php?route=information/information&information_id=8" class="nav-menu__link">Видео</a></li>
            <li class="nav-menu__item"><a href="index.php?route=information/information&information_id=9" class="nav-menu__link">Вакансии</a></li>
            <li class="nav-menu__item"><a href="index.php?route=information/contact" class="nav-menu__link">Контакты</a></li>
        </ul>
        <div class="contact contact--header">
            <div class="cart_adds">
                <?php if ($logged) { ?>
                <div class="personal personal-in">
                    <a href="index.php?route=account/logout" class="logout"></a>
                    <a href="index.php?route=account/account"
                       class="personal__link personal__link--signin personal__signin--in">Здравствуйте,&nbsp;<span><?php echo $firstname; ?></span></a>
                    <a href="index.php?route=checkout/checkout" class="personal__link personal__link--cart">
                        <div class="count-wrap"><span><?php echo $text_items; ?></span></div>
                    </a>
                </div>
                <a href="tel:<?= $telephone[1]; ?><?= $telephone[2]; ?>"
                   class="phone phone--header"><span>(<?= $telephone[1]; ?>)</span><?= $telephone[2]; ?></a>
                <?php }else{ ?>
                <div class="personal">
                    <a href="#login-form" class="personal__link personal__link--login personal__login--fixed popup"
                       data-effect="mfp-zoom-in"><span>Вход</span></a>
                    <a href="#signin-form" class="personal__link personal__link--signin personal__signin--fixed popup"
                       data-effect="mfp-zoom-in"><span>Регистрация</span></a>
                    <a href="index.php?route=checkout/checkout"
                       class="personal__link personal__link--cart personal__cart--fixed">
                        <div class="count-wrap"><span><?php echo $text_items; ?></span></div>
                    </a>
                </div>
                <a href="tel:<?= $telephone[1]; ?><?= $telephone[2]; ?>"
                   class="phone phone--header"><span>(<?= $telephone[1]; ?>)</span><?= $telephone[2]; ?></a>
                <?php } ?>
            </div>
        </div>


    </div>
    </div>
    <?php if(($currentRout == 'common/home')||($currentRout == '')) { ?>

    <div class="header-main clearfix">
        <div class="container">
            <a href="<?php echo $home; ?>" class="logo">
                <img class="logo__pict" src="<?php echo $logo; ?>" alt="">
                <div class="logo__title">Novastom<span class="logo__accent">5 лет успешной работы</span></div>
            </a>

            <div class="search">
                <?php echo $search; ?>
            </div>

            <a href="#present-form" class="order-present popup" data-effect="mfp-zoom-in">Заказать<span>бесплатную презентацию</span></a>

            <a href="#callback-form" class="callback callback--header popup" data-effect="mfp-zoom-in">Заказать<span>обратный звонок</span></a>
            <span><?= $cart?></span>
        </div>
    </div>
    <?php if(!empty($data['modules'][0])){ ?>
    <?php echo $data['modules'][0]; ?>
    <?php }?>
    <div class="subscribe">
        <div class="subscribe__title">Подпишитесь на наши акции:</div>
        <div class="subscribe-in">
            <input type="text" id="subscribe" name="subscribe" placeholder="Ваш e-mail">
            <button id="subscribe_h" class="subscribe-btn">subscribe<i></i></button>
        </div>
    </div>
    <?php } else { ?>
    <div class="header-main header-main--inner clearfix">
        <div class="container">
            <a href="<?php echo $home; ?>" class="logo logo-inner">
                <img class="logo__pict logo__pict--inner" src="<?php echo $logo; ?>" alt="">
                <div class="logo__title logo__title--inner">Novastom<span class="logo__accent logo__accent--inner">5 лет успешной работы</span>
                </div>
            </a>

            <div class="search search--inner">
                <?php echo $search; ?>
            </div>

            <a href="#present-form" class="order-present order-present--inner popup"
               data-effect="mfp-zoom-in">Заказать<span>бесплатную презентацию</span></a>

            <a href="#callback-form" class="callback callback--header callback--inner popup" data-effect="mfp-zoom-in">Заказать<span>обратный звонок</span></a>

            <span><?= $cart?></span>
        </div>
    </div>
    <?php } ?>
</header>

<div class="hidden">
    <div id="present-form" class="form-block form-block--modal mfp-with-anim">
        <div class="form-block__title">Заказать бесплатную презентацию товара</div>
        <form action="#" id="contactForm_c">
            <div class="form-block__in form-block__in--name"><input id="name-1" name="name" type="text"
                                                                    placeholder="Ваше имя"></div>
            <div class="form-block__in form-block__in--phone"><input id="phone-1" name="phone" type="text"
                                                                     placeholder="Ваш телефон"></div>
            <button class="btn btn--form" id="send">Заказать презентацию</button>
        </form>
    </div>

    <div id="callback-form" class="form-block form-block--modal mfp-with-anim">
        <div class="form-block__title">Заказать<br>обратный звонок</div>
        <form action="#" id="callback-form-2">
            <div class="form-block__in form-block__in--name"><input class="name-2" name="name" type="text"
                                                                    placeholder="Ваше имя"></div>
            <div class="form-block__in form-block__in--phone"><input class="phone-2" name="phone" type="text"
                                                                     placeholder="Ваш телефон"></div>
            <button class="btn btn--form" id="callback-button">Заказать звонок</button>
        </form>
    </div>
    <div id="login-form" class="modal-form mfp-with-anim">
        <div class="modal-form__title modal-form__title--login"><i></i>Войти</div>
        <form action="#" id="login">
            <input name="login" type="text" placeholder="Логин">
            <input name="password" type="password" placeholder="Пароль">
            <button id="button-login" class="btn btn--form2">Войти</button>
        </form>
    </div>

    <div id="signin-form" class="modal-form modal-form--signin mfp-with-anim">
        <div class="modal-form__title modal-form__title--signin"><i></i>Регистрация</div>
        <form action="#" id="register">

            <input type="text" id="firstname" name="firstname" placeholder="Ваше имя">
            <?php if ($error_firstname) { ?>
            <div class="text-danger"><?php echo $error_firstname; ?></div>
            <?php } ?>

            <input type="text" id="loginReg" name="loginReg" placeholder="Логин">

            <input type="password" id="password" name="password" placeholder="Пароль">


            <input type="password" id="confirm" name="confirm" placeholder="Подтверждение пароля">


            <input type="text" id="email" name="email" placeholder="E-mail">

            <button id="button_register" class="btn btn--form2">Зарегистрироваться</button>
        </form>
    </div>
</div>
<div class="loader">
    <div class="loader_inner"></div>
</div>
<script>
    $('#button-login').on('click', function (even) {
        even.preventDefault();
        var res = $('#login').serializeArray();
        var arr = {};
        $.each(res, function (result) {
            var $index = res[result].name;
            arr[$index] = res[result].value;

        });
        $.ajax({
            url: 'index.php?route=account/login/login',
            type: 'POST',
            dataType: 'json',
            data: res,
            success: function (data) {
                if (data.status == true) {
                    document.location.replace("/index.php?route=account/account");
                } else {
                    swal("Вы ввели неверные данные", "", "error");
                }
            }
        });
    });
</script>

<script>
    $('#button_register').on('click', function (even) {
        even.preventDefault();
        var res = $('#register').serializeArray();
        var arr = {};

        $.each(res, function (result) {
            var $index = res[result].name;
            arr[$index] = res[result].value;
        });
        $.ajax({
            url: 'index.php?route=account/register/register',
            type: 'post',
            dataType: 'json',
            data: res,
            success: function (data) {
                if (data.status == true) {
                    swal("Вы успешно зарегистрировались, пожалуйста выполните вход!", "", "success");
                    $.magnificPopup.close();


                    var arr = {
                        'loginReg': $('#loginReg').val(),
                        'email':$('#email').val()
                };
                        $.ajax({
                            url: 'index.php?route=common/header/registerForm',
                            type: 'post',
                            dataType: 'json',
                            data: arr,
                            success: function () {
                            }
                        });
                    $('#firstname').val('');
                    $('#loginReg').val('');
                    $('#password').val('');
                    $('#confirm').val('');
                    $('#email').val('');
                } else {
                    if(data.firstname){
                        swal("Имя должно содержать от 1 до 32 символов!", "", "error");
                    } else if(data.loginReg){
                        swal("Логин должно содержать от 1 до 32 символов!", "", "error");
                    }else if(data.password){
                        swal("В пароле должно быть от 4 до 20 символов!", "", "error");
                    }else if(data.confirm){
                        swal("Пароли не совпадают!", "", "error");
                    }else if(data.email){
                        swal("E-Mail введён неправильно!", "", "error");
                    }else if(data.warning){
                        swal("Этот E-Mail уже зарегистрирован!", "", "error");
                    }else if(data.warning2){
                        swal("Этот Логин уже зарегистрирован!", "", "error");
                    }
                }
            }
        });

    });



(function($) {

    $.fn.menuAim = function(opts) {
        // Initialize menu-aim for all elements in jQuery collection
        this.each(function() {
            init.call(this, opts);
        });

        return this;
    };

    function init(opts) {
        var $menu = $(this),
            activeRow = null,
            mouseLocs = [],
            lastDelayLoc = null,
            timeoutId = null,
            options = $.extend({
                rowSelector: "> li",
                submenuSelector: "*",
                submenuDirection: "right",
                tolerance: 75,  // bigger = more forgivey when entering submenu
                enter: $.noop,
                exit: $.noop,
                activate: $.noop,
                deactivate: $.noop,
                exitMenu: $.noop
            }, opts);

        var MOUSE_LOCS_TRACKED = 3,  // number of past mouse locations to track
            DELAY = 300;  // ms delay when user appears to be entering submenu

        /**
         * Keep track of the last few locations of the mouse.
         */
        var mousemoveDocument = function(e) {
                mouseLocs.push({x: e.pageX, y: e.pageY});

                if (mouseLocs.length > MOUSE_LOCS_TRACKED) {
                    mouseLocs.shift();
                }
            };

        /**
         * Cancel possible row activations when leaving the menu entirely
         */
        var mouseleaveMenu = function() {
                if (timeoutId) {
                    clearTimeout(timeoutId);
                }

                // If exitMenu is supplied and returns true, deactivate the
                // currently active row on menu exit.
                if (options.exitMenu(this)) {
                    if (activeRow) {
                        options.deactivate(activeRow);
                    }

                    activeRow = null;
                }
            };

        /**
         * Trigger a possible row activation whenever entering a new row.
         */
        var mouseenterRow = function() {
                if (timeoutId) {
                    // Cancel any previous activation delays
                    clearTimeout(timeoutId);
                }

                options.enter(this);
                possiblyActivate(this);
            },
            mouseleaveRow = function() {
                options.exit(this);
            };

        /*
         * Immediately activate a row if the user clicks on it.
         */
        var clickRow = function() {
                activate(this);
            };

        /**
         * Activate a menu row.
         */
        var activate = function(row) {
                if (row == activeRow) {
                    return;
                }

                if (activeRow) {
                    options.deactivate(activeRow);
                }

                options.activate(row);
                activeRow = row;
            };

        var possiblyActivate = function(row) {
                var delay = activationDelay();

                if (delay) {
                    timeoutId = setTimeout(function() {
                        possiblyActivate(row);
                    }, delay);
                } else {
                    activate(row);
                }
            };

    
        var activationDelay = function() {
                if (!activeRow || !$(activeRow).is(options.submenuSelector)) {
                    // If there is no other submenu row already active, then
                    // go ahead and activate immediately.
                    return 0;
                }

                var offset = $menu.offset(),
                    upperLeft = {
                        x: offset.left,
                        y: offset.top - options.tolerance
                    },
                    upperRight = {
                        x: offset.left + $menu.outerWidth(),
                        y: upperLeft.y
                    },
                    lowerLeft = {
                        x: offset.left,
                        y: offset.top + $menu.outerHeight() + options.tolerance
                    },
                    lowerRight = {
                        x: offset.left + $menu.outerWidth(),
                        y: lowerLeft.y
                    },
                    loc = mouseLocs[mouseLocs.length - 1],
                    prevLoc = mouseLocs[0];

                if (!loc) {
                    return 0;
                }

                if (!prevLoc) {
                    prevLoc = loc;
                }

                if (prevLoc.x < offset.left || prevLoc.x > lowerRight.x ||
                    prevLoc.y < offset.top || prevLoc.y > lowerRight.y) {
                    // If the previous mouse location was outside of the entire
                    // menu's bounds, immediately activate.
                    return 0;
                }

                if (lastDelayLoc &&
                        loc.x == lastDelayLoc.x && loc.y == lastDelayLoc.y) {
                    return 0;
                }

                function slope(a, b) {
                    return (b.y - a.y) / (b.x - a.x);
                };

                var decreasingCorner = upperRight,
                    increasingCorner = lowerRight;

                if (options.submenuDirection == "left") {
                    decreasingCorner = lowerLeft;
                    increasingCorner = upperLeft;
                } else if (options.submenuDirection == "below") {
                    decreasingCorner = lowerRight;
                    increasingCorner = lowerLeft;
                } else if (options.submenuDirection == "above") {
                    decreasingCorner = upperLeft;
                    increasingCorner = upperRight;
                }

                var decreasingSlope = slope(loc, decreasingCorner),
                    increasingSlope = slope(loc, increasingCorner),
                    prevDecreasingSlope = slope(prevLoc, decreasingCorner),
                    prevIncreasingSlope = slope(prevLoc, increasingCorner);

                if (decreasingSlope < prevDecreasingSlope &&
                        increasingSlope > prevIncreasingSlope) {
                    lastDelayLoc = loc;
                    return DELAY;
                }

                lastDelayLoc = null;
                return 0;
            };

        $menu
            .mouseleave(mouseleaveMenu)
            .find(options.rowSelector)
                .mouseenter(mouseenterRow)
                .mouseleave(mouseleaveRow)
                .click(clickRow);

        $(document).mousemove(mousemoveDocument);

    };
})(jQuery);

jQuery(document).ready(function($){

	//on mobile - open submenu
	$('.has-children').children('a').on('click', function(event){
		//prevent default clicking on direct children of .has-children 
		event.preventDefault();
		var selected = $(this);
		selected.next('ul').removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('move-out');
	});

	//on desktop - differentiate between a user trying to hover over a dropdown item vs trying to navigate into a submenu's contents
	var submenuDirection = ( !$('.cd-dropdown-wrapper').hasClass('open-to-left') ) ? 'right' : 'left';
	$('.cd-dropdown-content').menuAim({
        activate: function(row) {
        	$(row).children().addClass('is-active').removeClass('fade-out');
        	if( $('.cd-dropdown-content .fade-in').length == 0 ) $(row).children('ul').addClass('fade-in');
        },
        deactivate: function(row) {
        	$(row).children().removeClass('is-active');
        	if( $('li.has-children:hover').length == 0 || $('li.has-children:hover').is($(row)) ) {
        		$('.cd-dropdown-content').find('.fade-in').removeClass('fade-in');
        		$(row).children('ul').addClass('fade-out')
        	}
        },
        exitMenu: function() {
        	$('.cd-dropdown-content').find('.is-active').removeClass('is-active');
        	return true;
        },
        submenuDirection: submenuDirection,
    });

	//submenu items - go back link
	$('.go-back').on('click', function(){
		var selected = $(this),
			visibleNav = $(this).parent('ul').parent('.has-children').parent('ul');
		selected.parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('move-out');
	}); 

	function toggleNav(){
		var navIsVisible = ( !$('.cd-dropdown').hasClass('dropdown-is-active') ) ? true : false;
		$('.cd-dropdown').toggleClass('dropdown-is-active', navIsVisible);
		$('.cd-dropdown-trigger').toggleClass('dropdown-is-active', navIsVisible);
		if( !navIsVisible ) {
			$('.cd-dropdown').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',function(){
				$('.has-children ul').addClass('is-hidden');
				$('.move-out').removeClass('move-out');
				$('.is-active').removeClass('is-active');
			});	
		}
	}

	//IE9 placeholder fallback
	//credits http://www.hagenburger.net/BLOG/HTML5-Input-Placeholder-Fix-With-jQuery.html
	if(!Modernizr.input.placeholder){
		$('[placeholder]').focus(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
		  	}
		}).blur(function() {
		 	var input = $(this);
		  	if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.val(input.attr('placeholder'));
		  	}
		}).blur();
		$('[placeholder]').parents('form').submit(function() {
		  	$(this).find('[placeholder]').each(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
			 		input.val('');
				}
		  	})
		});
	}
});


</script>

