<?php echo $header; ?>
<div class="wrapper">
  <div class="catalog">
    <div class="container">
      <div class="catalog__title">Каталог продукции</div>
      <div class="catalog-carousel">
        <?php foreach ($categories as $category) { ?>
        <div class="catalog-banner">
          <a href="<?= $category['href']; ?>" class="catalog-banner__pict"><img src="<?= $category['image']; ?>" alt=""></a>
          <a href="<?= $category['href']; ?>" class="catalog-banner__title"><?= $category['name']; ?></a>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
  <div class="brend">
    <div class="container">
      <div class="brend__title">Бренды<span>Наши бренды</span></div>
      <div class="brend-carousel">
        <?php foreach ($result as $manufacturers) { ?>
        <div class="brend-item">
          <a class="brend-item__logo"><img src="<?=$manufacturers['image']; ?>" alt=""></a>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
  <?php echo $content_top; ?>
  <?php echo $content_bottom; ?>
  <div class="sale clearfix">
    <div class="container">
      <div class="sale-block">
        <div class="sale-text">
          <div class="sale-text__top">Скидка</div>
          <div class="sale-text__middle">на весь товар,</div>
          <div class="sale-text__bottom">при заказе презентации</div>
          <div class="sale-text__num">10<span>%</span></div>
        </div>
        <div class="form-block">
          <div class="form-block__title">Заказать бесплатную презентацию товара</div>
          <form action="#" id="present-form-2">
            <div class="form-block__in form-block__in--name"><input id="name-3" name="name" type="text" placeholder="Ваше имя"></div>
            <div class="form-block__in form-block__in--phone"><input id="phone-3" name="phone" type="text" placeholder="Ваш телефон"></div>
            <button class="btn btn--form" id="present-button-2">Заказать презентацию</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="about clearfix">
    <div class="container">
      <div class="about-block">
        <?php echo $column_right; ?>
      </div>
    </div>
  </div>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"></div>
    </div>
</div>
<?php echo $footer; ?>