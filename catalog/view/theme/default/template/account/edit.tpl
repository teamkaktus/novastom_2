<div class="container">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="profile">
        <div class="profile-title">Ваши персональные данные</div>
        <input id="<?=$customer_id; ?>" class="hidden">
        <ul class="profile-list">
            <li class="profile-list__item">
                <div class="profile-list__title">Имя:</div>
                <input data-filde="firstname" readonly="readonly" class="profile-list__descr input_style"
                       value="<?php echo $firstname; ?>"/>
                <a data-filde="firstname" style="cursor: pointer;" class="edit-link">edit</a>
                <a data-filde="firstname" style="cursor: pointer; display: none;" class="icon-save"></a>
            </li>
            <li class="profile-list__item">
                <div class="profile-list__title">Фамилия:</div>
                <input readonly="readonly" data-filde="lastname" class="profile-list__descr input_style"
                       value="<?php echo $lastname; ?>"/>
                <a data-filde="lastname" style="cursor: pointer" class="edit-link">edit</a>
                <a data-filde="lastname" style="cursor: pointer; display: none;" class="icon-save"></a>
            </li>
            <li class="profile-list__item">
                <div class="profile-list__title">Отчество:</div>
                <input readonly="readonly" data-filde="middle_name" class="profile-list__descr input_style"
                       value="<?php echo $middle_name; ?>"/>
                <a data-filde="middle_name" style="cursor: pointer" class="edit-link">edit</a>
                <a data-filde="middle_name" style="cursor: pointer; display: none;" class="icon-save"></a>
            </li>
            <li class="profile-list__item">
                <div class="profile-list__title">E-mail:</div>
                <input readonly="readonly" data-filde="email" class="profile-list__descr input_style"
                       value="<?php echo $email; ?>"/>
                <a data-filde="email" style="cursor: pointer" class="edit-link">edit</a>
                <a data-filde="email" style="cursor: pointer; display: none;" class="icon-save"></a>
            </li>
            <li class="profile-list__item">
                <div class="profile-list__title">Телефон:</div>
                <input readonly="readonly" data-filde="telephone" class="profile-list__descr input_style"
                       value="<?php echo $telephone; ?>"/>
                <a data-filde="telephone" style="cursor: pointer" class="edit-link">edit</a>
                <a data-filde="telephone" style="cursor: pointer; display: none;" class="icon-save"></a>
            </li>
            <li class="profile-list__item">
                <div class="profile-list__title">Пароль:</div>
                <input readonly="readonly" data-filde="password" class="profile-list__descr input_style" type="password"
                       />
                <a data-filde="password" style="cursor: pointer" class="edit-link">edit</a>
                <a data-filde="password" style="cursor: pointer; display: none;" class="icon-save"></a>
            </li>
        </ul>
    </div>
</div>
<script>

    $('.icon-save').on('click', function (e) {
        var self = this;
        var field = $(this).data('filde');
        var value = $('input[data-filde="' + field + '"]').val();
        $.ajax({
            url: "index.php?route=account/edit/edit_field",
            method: 'POST',
            dataType: 'JSON',
            cache: false,
            data: {
                field: field,
                value: value
            },
            success: function (data) {
                swal("Ваша запись была успешно отредактирована", "", "success");
                $('input[data-filde="' + field + '"]').attr("readonly", "readonly");
                $('input[data-filde="' + field + '"]').addClass("input_style");
                $(self).hide();
                $(self).prev('.edit-link').show();
            }
        })
    })
    $('.edit-link').on('click', function (e) {

        var field = $(this).data('filde');
        $('input[data-filde="' + field + '"]').removeAttr("readonly");
        $('input[data-filde="' + field + '"]').removeClass("input_style");
        $(this).hide();
        $(this).next('.icon-save').show();
    })
</script>
<script type="text/javascript"><!--
    // Sort the custom fields
    $('.form-group[data-sort]').detach().each(function () {
        if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length) {
            $('.form-group').eq($(this).attr('data-sort')).before(this);
        }

        if ($(this).attr('data-sort') > $('.form-group').length) {
            $('.form-group:last').after(this);
        }

        if ($(this).attr('data-sort') < -$('.form-group').length) {
            $('.form-group:first').before(this);
        }
    });
    //--></script>
<script type="text/javascript"><!--
    $('button[id^=\'button-custom-field\']').on('click', function () {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $(node).button('loading');
                    },
                    complete: function () {
                        $(node).button('reset');
                    },
                    success: function (json) {
                        $(node).parent().find('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>

