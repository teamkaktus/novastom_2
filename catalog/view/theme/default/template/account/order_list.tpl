<div class="container">
  <div class="profile profile--history">
    <div class="profile-title">История заказов</div>

    <div class="tb tb-profile">
      <div class="tb-row tb-row--head">
        <div class="tb-cell tb-cell--head">&nbsp;</div>
        <div class="tb-cell tb-cell--head">№ заказа и дата</div>
        <div class="tb-cell tb-cell--head">Сумма к оплате</div>
        <div class="tb-cell tb-cell--head">Статус заказа</div>
      </div>
      <?php foreach ($orders as $order) { ?>
      <div class="tb-row">
        <div class="tb-cell tb-cell--count"><?php echo $order['order_id']; ?></div>
        <div class="tb-cell tb-cell--order"><a href="<?php echo $order['href']; ?>">Заказ №<?php echo $order['order_id']; ?> от <?php echo $order['date_added']; ?></a></div>
        <div class="tb-cell tb-cell--price"><span>Сумма:</span><?php echo $order['total']; ?> руб.</div>
        <div class="tb-cell tb-cell--status"><span>Статус:</span><?php echo $order['status']; ?></div>
      </div>
      <?php } ?>
    </div>
  </div>
  </div>