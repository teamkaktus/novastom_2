<script>
	var config = <?php echo $json_config; ?>;
</script>
<style>
	<?php echo $config['design']['custom_style']; ?>
	<?php if($config['design']['only_quickcheckout']){ ?>
	body > *{
		display: none
	}
	body > #d_quickcheckout{
		display: block;
	}
	#d_quickcheckout.container #logo{
		margin: 20px 0px;
	}
	<?php } ?>
</style>
<div class="content content--cart clearfix">
    <div class="container" style="padding: 0">
        <div id="d_quickcheckout">
            <?php echo $field; ?>
            <div class="row">
                <div class="col-md-12"></div>
            </div>
            <div class="qc-col-0">
                <?php echo $login; ?>
                <?php echo $payment_address; ?>
                <?php echo $shipping_address; ?>
                <?php echo $shipping_method; ?>
                <?php echo $payment_method; ?>
                <?php echo $cart; ?>
                <?php echo $payment; ?>
                <?php echo $confirm; ?>
            </div>

            <div class="row">
                <div class="qc-col-1 col-md-<?php echo $config['design']['column_width'][1] ?>">
                </div>
                <div class="col-md-<?php echo $config['design']['column_width'][4] ?>">
                    <div class="row">
                        <div id="cart_view" class="qc-step"></div>
                    </div>
                    <div class="col-sm-12 mod_style" style="padding: 0%">
                        <div class="order__title">Оформление заказа</div>
                        <div class="qc-col-2 col-md-<?php echo round(($config['design']['column_width'][2] / $config['design']['column_width'][4])*12); ?> col-sm-<?php echo round(($config['design']['column_width'][2] / $config['design']['column_width'][4])*12); ?> col-xs-<?php echo round(($config['design']['column_width'][2] / $config['design']['column_width'][4])*12); ?> col_padd_2 order clearfix">
                                <div class="order-form">
                                    <div id="payment_address" class="qc-step"></div>
                                    <div id="confirm_view" class="qc-step hei_poz_rel"></div>
                                </div>
                            <div class="option-order">
                                <div id="shipping_method" class="col-sm-12 qc-step pay-form"  style="padding-left: 0;"></div>
                                <div id="payment_method" class="col-sm-12 qc-step delivery-form" style="padding-bottom: 0;padding-left: 0;"></div>
                            </div>
                        </div>
                        <div class="qc-col-3 col-md-<?php echo round(($config['design']['column_width'][3] / $config['design']['column_width'][4])*12); ?>">
                        </div>
                        <div class="qc-col-4 col-md-12">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	$(function() {
		$('.qc-step').each(function(){
			$(this).appendTo('.qc-col-' + $(this).attr('data-col'));
		})
		$('.qc-step').tsort({attr:'data-row'});
		<?php if($config['design']['only_quickcheckout']){ ?>
			$('body').prepend($('#d_quickcheckout'));
			$('#d_quickcheckout').addClass('container')
			$('#d_quickcheckout #logo ').prepend($('header #logo').html())
			<?php } ?>
	})
</script>