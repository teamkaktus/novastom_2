<div class="news clearfix">
	<div class="container">
		<div class="news__title"><?php echo $heading_title; ?></div>
		<?php foreach ($news as $news_item) { ?>
		<div class="news-item">
			<div class="news-item__pict"><img src="<?php echo $news_item['thumb']; ?>" alt="<?php echo $news_item['title']; ?>"></div>
			<div class="news-item__date"><span><?= $news_item['posted_day']; ?></span>&nbsp;<?= $news_item['month_ru']; ?></div>
			<a href="<?php echo $news_item['href']; ?>" class="news-item__title"><?php echo $news_item['title']; ?></a>
			<div class="news-item__descr">
				<p><?php echo $news_item['description']; ?></p>
			</div>
			<a href="<?php echo $news_item['href']; ?>" class="btn btn--read"><span>Читать далее<i></i></span></a>
		</div>
		<?php } ?>
	</div>
</div>
