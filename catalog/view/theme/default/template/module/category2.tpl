<div class="sidebar">
    <div class="sidebar-title">Каталог</div>
    <ul class="catalog-nav">
        <?php foreach ($categories as $key => $category_1) { ?>
        <?php if ($key == 0) { ?>
        <li class="catalog-nav__item active"><a href="<?php echo $category_1['href'] ; ?>"
                                         class="catalog-nav__link active"><i></i><?php echo $category_1['name'] ; ?></a>
            <?php if(!empty($category_1['children'])){ ?>
            <ul class="catalog-dropnav catalog-dropnav22">
                <?php foreach ($category_1['children'] as $category_2) { ?>
                <?php if(!empty($category_2['children'])){ ?>
                <li class="catalog-dropnav__item active">
                    <a href="<?= $category_2['href']; ?>" class="active">
                        <i></i>
                        <?php } else { ?>
                <li class="catalog-dropnav__item2">
                    <a href="<?= $category_2['href']; ?>" class="active">
                        <i></i>
                        <?php } ?>
                        <?= $category_2['name']; ?>
                    </a>
                    <?php if(!empty($category_2['children'])){ ?>
                    <ul class="catalog-subnav catalog-dropnav22">
                        <?php foreach ($category_2['children'] as $category_3) { ?>
                        <li class="catalog-subnav__item"><a
                                    href="<?= $category_3['href']; ?>"><?= $category_3['name']; ?></a></li>
                        <?php } ?>
                    </ul>
                    <?php } ?>
                </li>
                <?php } ?>
            </ul>
            <?php } ?>
        </li>
        <?php } else if ($key >= 1) { ?>
        <li class="catalog-nav__item"><a href="<?php echo $category_1['href'] ; ?>"
                                         class="catalog-nav__link"><i></i><?php echo $category_1['name'] ; ?></a>
            <?php if(!empty($category_1['children'])){ ?>
            <ul class="catalog-dropnav">
                <?php foreach ($category_1['children'] as $category_2) { ?>
                <?php if(!empty($category_2['children'])){ ?>
                <li class="catalog-dropnav__item">
                    <a href="<?= $category_2['href']; ?>">
                        <i></i>
                        <?php } else { ?>
                <li class="catalog-dropnav__item2">
                    <a href="<?= $category_2['href']; ?>" class="active">
                        <i></i>
                        <?php } ?>
                        <?= $category_2['name']; ?></a>
                    <?php if(!empty($category_2['children'])){ ?>
                    <ul class="catalog-subnav">
                        <?php foreach ($category_2['children'] as $category_3) { ?>
                        <li class="catalog-subnav__item"><a
                                    href="<?= $category_3['href']; ?>"><?= $category_3['name']; ?></a></li>
                        <?php } ?>
                    </ul>
                    <?php } ?>
                </li>
                <?php } ?>
            </ul>
            <?php } ?>
        </li>
        <?php } ?>
        <?php } ?>
    </ul>
</div>

