<!-- 
	Ajax Quick Checkout 
	v6.0.0
	Dreamvention.com 
	d_quickcheckout/payment_method.tpl 
-->
<script type="text/html" id="payment_method_template" >
	<form id="payment_method_form" <%= parseInt(model.config.display) ? '' : 'class="hidden"' %>>
	<% if (model.error_warning) { %>
	<div class="error"><%= model.error_warning %></div>
	<% } %>
	<% if (model.payment_methods) { %>
	<div class="pay-form">
		<div class="pay-form__title">Способ оплаты</div>
		<div class="panel-body" style="padding: 0;">
			<% if(model.error){ %>
			<div class="alert alert-danger">
				<i class="fa fa-exclamation-circle"></i> <%= model.error %>
			</div>
			<% } %>
			<div id="payment_method_list" class="<%= parseInt(model.config.display_options) ? '' : 'hidden' %>">
				<% if(model.config.input_style == 'select') { %>
				<div class="select-input form-group">
					<select name="payment_method" class="form-control payment-method-select" data-refresh="6" >
						<% _.each(model.payment_methods, function(payment_method) { %>
						<% if (payment_method.code == model.payment_method.code) { %>
						<option  value="<%= payment_method.code %>" id="<%= payment_method.code %>" selected="selected" ><%= payment_method.title %> <span class="price"><%= (payment_method.cost) ? payment_method.cost : '' %></span></option>
						<% } else { %>
						<option  value="<%= payment_method.code %>" id="<%= payment_method.code %>" ><%= payment_method.title %> <span class="price"><%= (payment_method.cost) ? payment_method.cost : '' %></span></option>
						<% } %>
						<% }) %>
					</select>
				</div>
				<% }else{ %>
				<% _.each(model.payment_methods, function(payment_method) { %>
				<div class="radio-input radio">
					<% if (payment_method.code == model.payment_method.code) { %>
					<input type="radio" name="payment_method" value="<%= payment_method.code %>" id="<%= payment_method.code %>" checked="checked" class="styled styled22"  data-refresh="6"/>
					<% } else { %>
					<input type="radio" name="payment_method" value="<%= payment_method.code %>" id="<%= payment_method.code %>" class="styled styled22"  data-refresh="6"/>
					<% } %>
					<label for="<%= payment_method.code %>">
						<div class="pay pay-1">
						<div class="pay-ic">
							<div class="hidden-xs hidden-sm heid_55">
								<% if(parseInt(model.config.display_images)) { %>
								<img class="payment-image" src="<%= payment_method.image %>" />
								<% } %>
							</div>
							<div class="hidden-lg hidden-md heid_55">
								<% if(parseInt(model.config.display_images)) { %>
								<img class="payment-image" src="<%= payment_method.image2 %>" />
								<% } %>
							</div>

							<span><%= payment_method.title %></span>
						</div>
						</div>
					</label>
				</div>
				</div>
				<% }) %>
				<% } %>
			</div>
		</div>
	</div>
	<% } %>
	</form>
</script>
<script>
	$(function() {
		qc.paymentMethod = $.extend(true, {}, new qc.PaymentMethod(<?php echo $json; ?>));
		qc.paymentMethodView = $.extend(true, {}, new qc.PaymentMethodView({
			el:$("#payment_method"),
			model: qc.paymentMethod,
			template: _.template($("#payment_method_template").html())
		}));
	});
</script>