<!--
	Ajax Quick Checkout
	v6.0.0
	Dreamvention.com
	d_quickcheckout/payment_address.tpl
-->
<script type="text/html" id="payment_address_template">
	<div class="<%= parseInt(model.config.display) ? '' : 'hidden' %>">
		<div class="panel panel-default">
			<div class="panel-heading" style="background: none">
				<div class="order-form__title">Личные данные</div>
			</div>
			<div class="panel-body" style="padding-left: 0;padding-top: 0;">
				<% if(model.account == 'logged'){ %>
				<form id="payment_address_form" class="form-horizontal">
				</form>
				<% }else{ %>
				<form id="payment_address_form" class="form-horizontal">
				</form>
				<% } %>
			</div>
		</div>
	</div>
</script>

<script>
	$(function() {
		qc.paymentAddress = $.extend(true, {}, new qc.PaymentAddress(<?php echo $json; ?>));
		qc.paymentAddressView = $.extend(true, {}, new qc.PaymentAddressView({
			el:$("#payment_address"),
			model: qc.paymentAddress,
			template: _.template($("#payment_address_template").html())
		}));
		qc.paymentAddressView.setZone(qc.paymentAddress.get('payment_address.country_id'));

	});
</script>