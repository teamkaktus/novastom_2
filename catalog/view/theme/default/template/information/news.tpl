<?php echo $header; ?>
<div class="content clearfix">
<div class="container">
	<div class="breadcrumb">
		<ul class="breadcrumb-list">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		</ul>
	</div>
	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
			<div class="page-title page-title--profile"><?php echo $heading_title; ?></div>
			<div class="news-article2 clearfix">
				<?php if ($thumb) { ?>
					<img src="<?php echo $thumb; ?>"/>
				<?php } ?>
					<?php echo $description; ?>
			</div>
		<?php echo $content_bottom; ?></div>
	<?php echo $column_right; ?></div>
	<script type="text/javascript"><!--
		$(document).ready(function () {
			$('.thumbnail').magnificPopup({
				type: 'image',
				delegate: 'a',
			});
		});
	//--></script>
</div>
</div>
<?php echo $footer; ?>