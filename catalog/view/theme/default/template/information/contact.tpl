<?php echo $header; ?>
<div class="container">
  <div class="breadcrumb">
    <ul class="breadcrumb-list">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div class="">
      <div class="block__title">Контактные данные</div>
      <div class="panel panel-default">
        <div class="panel-body">
          <div>
            <div style="margin-bottom: 20px"><strong class="telephone_style"><?php echo $text_telephone; ?>:</strong>
              <span class="telephone_style_1"><?php echo $telephone; ?>, <?php echo $telephone2; ?></span>
            </div>
            <div style="margin-bottom: 40px"><strong class="telephone_style">Email:</strong>
              <span class="telephone_style_1"><?php echo $config_email; ?></span>
            </div>
          </div>
        </div>
      </div>
      <div>
        <address class="block__title" style="margin-bottom: 20px;">
          <?php echo $address; ?>
        </address>
        <div id="map" style="width: 100%; height: 400px; margin-bottom: 20px" ></div>
      </div>
      <form id="kap_contact" style="margin-top: 20px;     text-align: center;" action="" method="post" enctype="multipart/form-data" class="form-horizontal">
        <span class="block__title">Задать вопрос</span>
        <div class="row" style="margin: 0; padding-left: 10px">
          <div>
            <span id="err_e_name" style="display: none; color: red; font-size: 20px;">Некоректно заполненно имя</span>
          </div>
          <div>
            <span id="err_e_email" style="display: none; color: red; font-size: 20px;">Некоректно введен E-mail</span>
          </div>
          <div>
            <span id="err_e_massage" style="display: none; color: red; font-size: 20px;">Некоректно введенно сообщение</span>
          </div>
        </div>
        <div class="form-group required" style="margin-top: 20px">
          <label class="control-label telephone_style" ><?php echo $entry_name; ?></label>
          <div class="">
            <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="input_style_contact" />
            <?php if ($error_name) { ?>
            <div class="text-danger"><?php echo $error_name; ?></div>
            <?php } ?>
          </div>
        </div>
        <div class="form-group required" style="margin-top: 20px">
          <label class=" control-label telephone_style" for="input-email"><?php echo $entry_email; ?></label>
          <div class="">
            <input type="text" name="email" value="<?php echo $email; ?>"  class="input_style_contact" />
            <?php if ($error_email) { ?>
            <div class="text-danger"><?php echo $error_email; ?></div>
            <?php } ?>
          </div>
        </div>
        <div class="form-group required" style="margin-top: 20px">
          <label class=" control-label telephone_style" for="input-enquiry"><?php echo $entry_enquiry; ?></label>
          <div class="">

            <textarea name="enquiry" rows="10"  class="input_style_contact2"><?php echo $enquiry; ?></textarea>
            <?php if ($error_enquiry) { ?>
            <div class="text-danger"><?php echo $error_enquiry; ?></div>
            <?php } ?>
          </div>
        </div>
        <?php echo $captcha; ?>
        <div class="buttons">
          <div style="margin-top: 20px; margin-bottom: 35px">
            <button id="send_contact" style="margin: auto" class="btn btn-primary"><?php echo $button_submit; ?></button>
          </div>
        </div>
      </form>
    </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
<script type="text/javascript">
  ymaps.ready(init);
  var myMap,
          myPlacemark;

  function init(){
    myMap = new ymaps.Map("map", {
      center: [55.544255, 37.523240],
      zoom: 16
    });

    myPlacemark = new ymaps.Placemark([55.544255, 37.523240], {
      hintContent: 'Москва!',
      balloonContent: 'Столица России'
    });

    myMap.geoObjects.add(myPlacemark);
  }
</script>
<?php echo $footer; ?>
