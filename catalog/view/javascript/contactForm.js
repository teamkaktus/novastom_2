$(document).ready(function () {
    $('#send').on('click', function (even) {
        even.preventDefault();
            var res = $('#contactForm_c').serializeArray();
            var arr = {};
            $.each(res, function (result) {
                var $index = res[result].name;
                arr[$index] = res[result].value;
            });
                 $('#name-1').val('');
                 $('#phone-1').val('');
            swal("Сообщение отправлено", "", "success");
            $.magnificPopup.close();
            $.ajax({
                url: 'index.php?route=common/header/contactForm',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function () {
                }
            });
    });

    $('#callback-button').on('click', function (even) {
        even.preventDefault();
            var res = $('#callback-form-2').serializeArray();
            var arr = {};
            $.each(res, function (result) {
                var $index = res[result].name;
                arr[$index] = res[result].value;
            });
        $('.name-2').val('');
        $('.phone-2').val('');
            swal("Сообщение отправлено", "", "success");

            $.magnificPopup.close();
            $.ajax({
                url: 'index.php?route=common/header/contactForm',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function () {
                }
            });

    });

    $('#present-button-2').on('click', function (even) {
        even.preventDefault();
            var res = $('#present-form-2').serializeArray();
            var arr = {};
            $.each(res, function (result) {
                var $index = res[result].name;
                arr[$index] = res[result].value;
            });
            swal("Сообщение отправлено", "", "success");
            $.magnificPopup.close();
            $.ajax({
                url: 'index.php?route=common/home/contactForm2',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function () {
                }
            });
            $('#name-3').val('');
            $('#phone-3').val('');
    });

    $('#subscribe_h').on('click', function (even) {
        even.preventDefault();
            var res = $('#subscribe');
            var arr = {};
            $.each(res, function (result) {
                var $index = res[result].name;
                arr[$index] = res[result].value;
            });
            swal("Сообщение отправлено", "", "success");
            $.ajax({
                url: 'index.php?route=common/header/contactForm1',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function () {
                }
            });
            $('#subscribe').val('');
    });


    $('#subscribe_footer').on('click', function (even) {
        even.preventDefault();
            var res = $('#subscribe_f');
            var arr = {};
            $.each(res, function (result) {
                var $index = res[result].name;
                arr[$index] = res[result].value;
            });
            swal("Сообщение отправлено", "", "success");
            $.ajax({
                url: 'index.php?route=common/footer/contactForm3',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function () {
                }
            });
            $('#subscribe_f').val('');
    });

});
