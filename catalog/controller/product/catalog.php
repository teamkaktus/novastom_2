<?php

class ControllerProductCatalog extends Controller
{
    public function index($setting)
    {
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('Главная'),
            'href' => $this->url->link('common/home')
        );
       

        $this->load->language('product/catalog');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $data['search'] = $this->load->controller('common/search');



        $this->load->model('extension/extension');
        $this->load->model('tool/image');

        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $data['categories'] = array();

        
        $path = '';

			$parts = explode('_', (string)$this->request->get['path']);
        $category_id = (int)array_pop($parts);

        $data['categories_ch'] = array();
        $categories_ch = $this->model_catalog_category->getCategories($category_id);
        
        foreach ($categories_ch as $category_ch) {
                    if ($category_ch['category_special']) {
                    $data['categories_ch'][] = array(
                        'name' => $category_ch['name'],
                        'href' => $this->url->link('product/catalog_product', 'path=' . $category_ch['category_id'] . '_' . $category_ch['category_id'].'&'.'category=' . $category_id),
                        'image'    => '/image/'.$category_ch['image'],
                        );
                }
                }

           $data['breadcrumbs'] = array();
           

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('Главная'),
			'href' => $this->url->link('common/home')
		);

      

        $category = '';

			$categorys = explode('_', (string)$this->request->get['category']);

			$category_id = (int)array_pop($categorys);
			

				$category_info = $this->model_catalog_category->getCategory($category_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category_special', 'path=' . $category_id)
					);
			}

      

		if (isset($this->request->get['path'])) {
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);
			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = (int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/catalog', 'path=' . $path . '&'.'category=' . $category_id)
					);
				}
			}
		} else {
			$category_id = 0;
		}

        /*$data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);

        foreach ($categories as $category) {


                $children_data = array();
                $children = $this->model_catalog_category->getCategories($category['category_id']);
                foreach ($children as $child) {
                    $children_data[] = array(
                        'name' => $child['name'],
                        'href' => $this->url->link('product/catalog_product', 'path=' . $category['category_id'] . '_' . $child['category_id']),
                        'image'    => '/image/'.$child['image'],
                    );
                }
        }
        $data['categories'] = $children_data;*/

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/cataog.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/catalog.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/product/catalog.tpl', $data));
        }
    }
}