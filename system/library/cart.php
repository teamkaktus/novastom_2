<?php
class Cart {
	private $data = array();

	public function __construct($registry) {
		$this->config = $registry->get('config');
		$this->customer = $registry->get('customer');
		$this->session = $registry->get('session');
		$this->db = $registry->get('db');
		$this->tax = $registry->get('tax');
		$this->weight = $registry->get('weight');

		// Remove all the expired carts with no customer ID
		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE customer_id = '0' AND date_added < DATE_SUB(NOW(), INTERVAL 1 HOUR)");

		if ($this->customer->getId()) {
			// We want to change the session ID on all the old items in the customers cart
			$this->db->query("UPDATE " . DB_PREFIX . "cart SET session_id = '" . $this->db->escape($this->session->getId()) . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");

			// Once the customer is logged in we want to update the customer ID on all items he has
			$cart_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '0' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");

			foreach ($cart_query->rows as $cart) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE cart_id = '" . (int)$cart['cart_id'] . "'");

				// The advantage of using $this->add is that it will check if the products already exist and increaser the quantity if necessary.
				$this->add($cart['product_id'], $cart['quantity'], json_decode($cart['option']), $cart['recurring_id']);
			}
		}
	}

	public function getProducts($owq_discount_disable = false) {
        $owq_total = $owq_discount_disable ? 0 : $this->owq_getTotal();
      
		$product_data = array();

		$cart_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");

		foreach ($cart_query->rows as $cart) {
			$stock = true;

			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store p2s LEFT JOIN " . DB_PREFIX . "product p ON (p2s.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND p2s.product_id = '" . (int)$cart['product_id'] . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.date_available <= NOW() AND p.status = '1'");

			if ($product_query->num_rows && ($cart['quantity'] > 0)) {
				$option_price = 0;
				$option_points = 0;
				$option_weight = 0;

				$option_data = array();

        $owq_total_discount = false;
      

				foreach (json_decode($cart['option']) as $product_option_id => $value) {
					$option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int)$product_option_id . "' AND po.product_id = '" . (int)$cart['product_id'] . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

					if ($option_query->num_rows) {
						if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {
							$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.form_option FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$value . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

							if ($option_value_query->num_rows) {
								if ($option_value_query->row['price_prefix'] == '+') {
									$option_price += $option_value_query->row['price'];
								} elseif ($option_value_query->row['price_prefix'] == '-') {
									$option_price -= $option_value_query->row['price'];
								}

								if ($option_value_query->row['points_prefix'] == '+') {
									$option_points += $option_value_query->row['points'];
								} elseif ($option_value_query->row['points_prefix'] == '-') {
									$option_points -= $option_value_query->row['points'];
								}


								if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart['quantity']))) {
									$stock = false;
								}

								$option_data[] = array(
									'product_option_id'       => $product_option_id,
									'product_option_value_id' => $value,
									'option_id'               => $option_query->row['option_id'],
									'option_value_id'         => $option_value_query->row['option_value_id'],
									'name'                    => $option_query->row['name'],
									'value'                   => $option_value_query->row['name'],
									'type'                    => $option_query->row['type'],
									'quantity'                => $option_value_query->row['quantity'],
									'subtract'                => $option_value_query->row['subtract'],
									'price'                   => $option_value_query->row['price'],
									'price_prefix'            => $option_value_query->row['price_prefix'],
									'points'                  => $option_value_query->row['points'],
									'points_prefix'           => $option_value_query->row['points_prefix'],
									'weight'                  => $option_value_query->row['weight'],
									'form_option'                  => $option_value_query->row['form_option'],
								);
							}
						} elseif ($option_query->row['type'] == 'checkbox' && is_array($value)) {
							foreach ($value as $product_option_value_id) {
								$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.form_option FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

								if ($option_value_query->num_rows) {
									if ($option_value_query->row['price_prefix'] == '+') {
										$option_price += $option_value_query->row['price'];
									} elseif ($option_value_query->row['price_prefix'] == '-') {
										$option_price -= $option_value_query->row['price'];
									}

									if ($option_value_query->row['points_prefix'] == '+') {
										$option_points += $option_value_query->row['points'];
									} elseif ($option_value_query->row['points_prefix'] == '-') {
										$option_points -= $option_value_query->row['points'];
									}


									if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart['quantity']))) {
										$stock = false;
									}

									$option_data[] = array(
										'product_option_id'       => $product_option_id,
										'product_option_value_id' => $product_option_value_id,
										'option_id'               => $option_query->row['option_id'],
										'option_value_id'         => $option_value_query->row['option_value_id'],
										'name'                    => $option_query->row['name'],
										'value'                   => $option_value_query->row['name'],
										'type'                    => $option_query->row['type'],
										'quantity'                => $option_value_query->row['quantity'],
										'subtract'                => $option_value_query->row['subtract'],
										'price'                   => $option_value_query->row['price'],
										'price_prefix'            => $option_value_query->row['price_prefix'],
										'points'                  => $option_value_query->row['points'],
										'points_prefix'           => $option_value_query->row['points_prefix'],
										'weight'                  => $option_value_query->row['weight'],
										'form_option'                  => $option_value_query->row['form_option'],
									);
								}
							}

        } elseif (($option_query->row['type'] == 'input_qty' || $option_query->row['type'] == 'input_qty_td') && is_array($value)) {
            foreach ($value as $product_option_value_id) {
                $option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.form_option, pov.image_option, pov.type_option, pov.sku FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
        
                if ($option_query->row['type'] == 'input_qty_td' && !$owq_discount_disable) {
                    $owq_total_discount = true;
                }

                if ($option_value_query->num_rows) {
                    if ($option_value_query->row['price_prefix'] == '+') {
                        $option_price += $option_value_query->row['price'];
                    } elseif ($option_value_query->row['price_prefix'] == '-') {
                        $option_price -= $option_value_query->row['price'];
                    }

                    if ($option_value_query->row['points_prefix'] == '+') {
                        $option_points += $option_value_query->row['points'];
                    } elseif ($option_value_query->row['points_prefix'] == '-') {
                        $option_points -= $option_value_query->row['points'];
                    }

                    
                    if (isset($cart['quantity'])) {
                        $quantity = $cart['quantity'];
                    }
                    
                    if (!empty($option_value_query->row['image'])) {
                        $product_query->row['image'] = $option_value_query->row['image'];
                    }
                    if (!empty($option_value_query->row['sku'])) {
                        $product_query->row['model'] = $option_value_query->row['sku'];
                    }

                    if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $quantity))) {
                        $stock = false;
                    }

                    $option_data[] = array(
                        'product_option_id'       => $product_option_id,
                        'product_option_value_id' => $product_option_value_id,
                        'option_id'               => $option_query->row['option_id'],
                        'option_value_id'         => $option_value_query->row['option_value_id'],
                        'name'                    => $option_query->row['name'],
                        'value'                   => $option_value_query->row['name'],
                        'type'                    => $option_query->row['type'],
                        'quantity'                => $option_value_query->row['quantity'],
                        'subtract'                => $option_value_query->row['subtract'],
                        'price'                   => $option_value_query->row['price'],
                        'price_prefix'            => $option_value_query->row['price_prefix'],
                        'points'                  => $option_value_query->row['points'],
                        'points_prefix'           => $option_value_query->row['points_prefix'],
                        'weight'                  => $option_value_query->row['weight'],
                        'form_option'                  => $option_value_query->row['form_option'],
                        'image_option'                  => $option_value_query->row['image_option'],
                        'type_option'                  => $option_value_query->row['type_option'],
                    );
                }
            }
        } elseif ($option_query->row['type'] == 'input_qty_ns' && is_array($value)) {
            foreach ($value as $option_value) {
                $opt_value_arr = explode('|', $option_value); 
                $product_option_value_id = $opt_value_arr[0];
                
                $option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
          
                $option_value_query->row['price'] *= $opt_value_arr[1];
                $owq_quantity = $opt_value_arr[1];

                if ($option_value_query->num_rows) {
                    if ($option_value_query->row['price_prefix'] == '+') {
                        $option_price += $option_value_query->row['price'];
                    } elseif ($option_value_query->row['price_prefix'] == '-') {
                        $option_price -= $option_value_query->row['price'];
                    }

                    if ($option_value_query->row['points_prefix'] == '+') {
                        $option_points += $option_value_query->row['points'];
                    } elseif ($option_value_query->row['points_prefix'] == '-') {
                        $option_points -= $option_value_query->row['points'];
                    }

                    if (isset($cart['quantity'])) {
                        $quantity = $cart['quantity'];
                    }

                    if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $quantity * $owq_quantity))) {
                        $stock = false;
                    }

                    $option_data[] = array(
                        'product_option_id'       => $product_option_id,
                        'product_option_value_id' => $product_option_value_id,
                        'option_id'               => $option_query->row['option_id'],
                        'option_value_id'         => $option_value_query->row['option_value_id'],
                        'name'                    => $option_query->row['name'],
                        'value'                   => $option_value_query->row['name'] . ' x ' . $opt_value_arr[1],
                        'owq_quantity'            => $owq_quantity,
                        'type'                    => $option_query->row['type'],
                        'quantity'                => $option_value_query->row['quantity'],
                        'subtract'                => $option_value_query->row['subtract'],
                        'price'                   => $option_value_query->row['price'],
                        'price_prefix'            => $option_value_query->row['price_prefix'],
                        'points'                  => $option_value_query->row['points'],
                        'points_prefix'           => $option_value_query->row['points_prefix'],
                        'weight'                  => $option_value_query->row['weight'],
                    );
                }
            }
        } elseif ($option_query->row['type'] == 'select_qty') {
            $opt_value_arr = explode('|', $value); 
            $product_option_value_id = $opt_value_arr[0];
            
            $option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
      
            $option_value_query->row['price'] *= $opt_value_arr[1];
            $owq_quantity = $opt_value_arr[1];

            if ($option_value_query->num_rows) {
                if ($option_value_query->row['price_prefix'] == '+') {
                    $option_price += $option_value_query->row['price'];
                } elseif ($option_value_query->row['price_prefix'] == '-') {
                    $option_price -= $option_value_query->row['price'];
                }

                if ($option_value_query->row['points_prefix'] == '+') {
                    $option_points += $option_value_query->row['points'];
                } elseif ($option_value_query->row['points_prefix'] == '-') {
                    $option_points -= $option_value_query->row['points'];
                }

                
                if (isset($cart['quantity'])) {
                    $quantity = $cart['quantity'];
                }

                if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $quantity * $owq_quantity))) {
                    $stock = false;
                }

                $option_data[] = array(
                    'product_option_id'       => $product_option_id,
                    'product_option_value_id' => $product_option_value_id,
                    'sku'   => (!empty($product_option_value['sku']) ? $product_option_value['sku'] : ''),
                    'form_option'             => $product_option_value['form_option'],
                    'option_id'               => $option_query->row['option_id'],
                    'option_value_id'         => $option_value_query->row['option_value_id'],
                    'name'                    => $option_query->row['name'],
                    'value'                   => $option_value_query->row['name'] . ' x ' . $opt_value_arr[1],
                    'owq_quantity'            => $owq_quantity,
                    'type'                    => $option_query->row['type'],
                    'quantity'                => $option_value_query->row['quantity'],
                    'subtract'                => $option_value_query->row['subtract'],
                    'price'                   => $option_value_query->row['price'],
                    'price_prefix'            => $option_value_query->row['price_prefix'],
                    'points'                  => $option_value_query->row['points'],
                    'points_prefix'           => $option_value_query->row['points_prefix'],
                    'weight'                  => $option_value_query->row['weight'],

                );
            }
      
						} elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
							$option_data[] = array(
								'product_option_id'       => $product_option_id,
								'product_option_value_id' => '',
								'option_id'               => $option_query->row['option_id'],
								'option_value_id'         => '',
								'name'                    => $option_query->row['name'],
								'value'                   => $value,
								'type'                    => $option_query->row['type'],
								'quantity'                => '',
								'subtract'                => '',
								'price'                   => '',
								'price_prefix'            => '',
								'points'                  => '',
								'points_prefix'           => '',
								'weight'                  => '',
                                'form_option'             => $option_query->row['form_option'],
							);
						}
					}
				}

				$price = $product_query->row['price'];

				// Product Discounts
				$discount_quantity = 0;

				$cart_2_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");

				foreach ($cart_2_query->rows as $cart_2) {
					if ($cart_2['product_id'] == $cart['product_id']) {
						$discount_quantity += $cart_2['quantity'];
					}
				}


        if ($owq_total_discount) $discount_quantity = $owq_total;
      
				$product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$cart['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND quantity <= '" . (int)$discount_quantity . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");

				if ($product_discount_query->num_rows) {

        if ((float)$product_discount_query->row['price'] && (float)$product_query->row['price'] && $this->config->get('config_autocalc_option_discount')) {
          $discount_coefficient = (float)$product_discount_query->row['price'] / (float)$product_query->row['price'];
          if (is_array($option_price)) {
            foreach($option_price as $key1=>$operations){
              foreach($operations as $operation=>$value){
                if (($operation == '+' || $operation == '-' || $operation == '=')) {
                  $option_price[$key1][$operation] = $value * $discount_coefficient;
                }
              }
            }
          } else {
            $option_price *= $discount_coefficient;
          }
        }
      
					
        if ($owq_total_discount) {
            $price *= 1.0 - $product_discount_query->row['price'] / 100.0;
            if (!is_array($option_price)) $option_price *= 1.0 - $product_discount_query->row['price'] / 100.0;
        } else {
            $price = $product_discount_query->row['price'];
        }
      
				}

				// Product Specials
				$product_special_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$cart['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC, price ASC LIMIT 1");

				if ($product_special_query->num_rows) {

        if ($product_special_query->row['price'] > 0.0 && $this->config->get('config_autocalc_option_special')) {
          $special_koefficient = (float)$product_query->row['price'] / (float)$product_special_query->row['price'];
          if (is_array($option_price)) {
            foreach($option_price as $key1=>$operations){
              foreach($operations as $operation=>$value){
                if (($operation == '+' || $operation == '-' || $operation == '=') && (float)$special_koefficient ) {
                  $option_price[$key1][$operation] = $value / $special_koefficient;
                }
              }
            }
          } else if ((float)$special_koefficient) {
            $option_price /= $special_koefficient;
          }
        }
      
					$price = $product_special_query->row['price'];
				}

				// Reward Points
				$product_reward_query = $this->db->query("SELECT points FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$cart['product_id'] . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'");

				if ($product_reward_query->num_rows) {
					$reward = $product_reward_query->row['points'];
				} else {
					$reward = 0;
				}

				// Downloads
				$download_data = array();

				$download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download p2d LEFT JOIN " . DB_PREFIX . "download d ON (p2d.download_id = d.download_id) LEFT JOIN " . DB_PREFIX . "download_description dd ON (d.download_id = dd.download_id) WHERE p2d.product_id = '" . (int)$cart['product_id'] . "' AND dd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

				foreach ($download_query->rows as $download) {
					$download_data[] = array(
						'download_id' => $download['download_id'],
						'name'        => $download['name'],
						'filename'    => $download['filename'],
						'mask'        => $download['mask']
					);
				}

				// Stock
				if (!$product_query->row['quantity'] || ($product_query->row['quantity'] < $cart['quantity'])) {
					$stock = false;
				}

				$recurring_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "recurring r LEFT JOIN " . DB_PREFIX . "product_recurring pr ON (r.recurring_id = pr.recurring_id) LEFT JOIN " . DB_PREFIX . "recurring_description rd ON (r.recurring_id = rd.recurring_id) WHERE r.recurring_id = '" . (int)$cart['recurring_id'] . "' AND pr.product_id = '" . (int)$cart['product_id'] . "' AND rd.language_id = " . (int)$this->config->get('config_language_id') . " AND r.status = 1 AND pr.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'");

				if ($recurring_query->num_rows) {
					$recurring = array(
						'recurring_id'    => $cart['recurring_id'],
						'name'            => $recurring_query->row['name'],
						'frequency'       => $recurring_query->row['frequency'],
						'price'           => $recurring_query->row['price'],
						'cycle'           => $recurring_query->row['cycle'],
						'duration'        => $recurring_query->row['duration'],
						'trial'           => $recurring_query->row['trial_status'],
						'trial_frequency' => $recurring_query->row['trial_frequency'],
						'trial_price'     => $recurring_query->row['trial_price'],
						'trial_cycle'     => $recurring_query->row['trial_cycle'],
						'trial_duration'  => $recurring_query->row['trial_duration']
					);
				} else {
					$recurring = false;
				}

				$product_data[] = array(
					'cart_id'         => $cart['cart_id'],
					'product_id'      => $product_query->row['product_id'],
					'name'            => $product_query->row['name'],
					'model'           => $product_query->row['model'],
					'shipping'        => $product_query->row['shipping'],
					'image'           => $product_query->row['image'],
					'option'          => $option_data,
					'download'        => $download_data,
					'quantity'        => $cart['quantity'],
					'minimum'         => $product_query->row['minimum'],
					'subtract'        => $product_query->row['subtract'],
					'stock'           => $stock,
					'price'           => ($price + $option_price),
					'total'           => ($price + $option_price) * $cart['quantity'],
					'reward'          => $reward * $cart['quantity'],
					'points'          => ($product_query->row['points'] ? ($product_query->row['points'] + $option_points) * $cart['quantity'] : 0),
					'tax_class_id'    => $product_query->row['tax_class_id'],
					'type_product'    => $product_query->row['type_product'],
					'type_product1'    => $product_query->row['type_product1'],
					'weight'          => ($product_query->row['weight'] + $option_weight) * $cart['quantity'],
					'weight_class_id' => $product_query->row['weight_class_id'],
					'length'          => $product_query->row['length'],
					'width'           => $product_query->row['width'],
					'height'          => $product_query->row['height'],
					'length_class_id' => $product_query->row['length_class_id'],
					'recurring'       => $recurring
				);
			} else {
				$this->remove($cart['cart_id']);
			}
		}

		return $product_data;
	}

	
        public function add($product_id, $qty = 1, $option = array(), $recurring_id = 0) {
            if (!$option) {
                $this->add_orig($product_id, $qty, array(), $recurring_id);
            } else {
                $option_single = array();
                $option_arr = array();
                
                foreach ($option as $option_key => $opt) {
                    $option_with_qty = false;
                    if (is_array($opt)) {
                        foreach ($opt as $option_value) {
                            if (strpos($option_value, '/') !== false) {
                                $option_with_qty = true;
                                break;
                            }
                        }
                    }
                    if ($option_with_qty) {
                        $option_arr[$option_key] = $opt;
                    } else {
                        $option_single[$option_key] = $opt;
                    } 
                }
                
                if (!$option_arr) {
                    $this->add_orig($product_id, $qty, $option_single, $recurring_id);
                } else {
                    foreach ($option_arr as $option_arr_key => $opt) {
                        foreach ($opt as $opt_key => $opt_val) {
                            $new_qty = $qty;
                            
                            $value = $opt_val;
                            
                            $split_value = explode('/', $opt_val);
                            if (count($split_value) == 2) {
                                $value = $split_value[0] . '/1';
                                $new_qty = (int)$split_value[1];
                            }
              
                            $new_option = $option_single;
                            $new_option[$option_arr_key] = array($value);                        
        
                            $this->add_orig($product_id, $new_qty, $new_option, $recurring_id);
                        }
                    }
                }
            }
        }
        
        private function add_orig($product_id, $quantity = 1, $option = array(), $recurring_id = 0) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "' AND product_id = '" . (int)$product_id . "' AND recurring_id = '" . (int)$recurring_id . "' AND `option` = '" . $this->db->escape(json_encode($option)) . "'");

		if (!$query->row['total']) {
			$this->db->query("INSERT " . DB_PREFIX . "cart SET customer_id = '" . (int)$this->customer->getId() . "', session_id = '" . $this->db->escape($this->session->getId()) . "', product_id = '" . (int)$product_id . "', recurring_id = '" . (int)$recurring_id . "', `option` = '" . $this->db->escape(json_encode($option)) . "', quantity = '" . (int)$quantity . "', date_added = NOW()");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "cart SET quantity = (quantity + " . (int)$quantity . ") WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "' AND product_id = '" . (int)$product_id . "' AND recurring_id = '" . (int)$recurring_id . "' AND `option` = '" . $this->db->escape(json_encode($option)) . "'");
		}
	}

	public function update($cart_id, $quantity) {
		$this->db->query("UPDATE " . DB_PREFIX . "cart SET quantity = '" . (int)$quantity . "' WHERE cart_id = '" . (int)$cart_id . "' AND customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
	}

	public function remove($cart_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE cart_id = '" . (int)$cart_id . "' AND customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
	}

	public function clear() {
		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
	}

	public function getRecurringProducts() {
		$product_data = array();

		foreach ($this->getProducts() as $value) {
			if ($value['recurring']) {
				$product_data[] = $value;
			}
		}

		return $product_data;
	}

	public function getWeight() {
		$weight = 0;

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				$weight += $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
			}
		}

		return $weight;
	}

	public function getSubTotal() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			$total += $product['total'];
		}

		return $total;
	}

	public function getTaxes() {
		$tax_data = array();

		foreach ($this->getProducts() as $product) {
			if ($product['tax_class_id']) {
				$tax_rates = $this->tax->getRates($product['price'], $product['tax_class_id']);

				foreach ($tax_rates as $tax_rate) {
					if (!isset($tax_data[$tax_rate['tax_rate_id']])) {
						$tax_data[$tax_rate['tax_rate_id']] = ($tax_rate['amount'] * $product['quantity']);
					} else {
						$tax_data[$tax_rate['tax_rate_id']] += ($tax_rate['amount'] * $product['quantity']);
					}
				}
			}
		}

		return $tax_data;
	}


        private function owq_getTotal() {
            $total = 0;

            foreach ($this->getProducts(true) as $product) {
                $total += $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
            }

            return $total;
        }
      
	public function getTotal() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			$total += $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
		}

		return $total;
	}

	public function countProducts() {
		$product_total = 0;

		$products = $this->getProducts();

		foreach ($products as $product) {
			$product_total += $product['quantity'];
		}

		return $product_total;
	}

	public function hasProducts() {
		return count($this->getProducts());
	}

	public function hasRecurringProducts() {
		return count($this->getRecurringProducts());
	}

	public function hasStock() {
		$stock = true;

		foreach ($this->getProducts() as $product) {
			if (!$product['stock']) {
				$stock = false;
			}
		}

		return $stock;
	}

	public function hasShipping() {
		$shipping = false;

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				$shipping = true;

				break;
			}
		}

		return $shipping;
	}

	public function hasDownload() {
		$download = false;

		foreach ($this->getProducts() as $product) {
			if ($product['download']) {
				$download = true;

				break;
			}
		}

		return $download;
	}
}
